<?php

namespace App\Service;

use Caxy\BaseX\Session;
use Caxy\BaseX\Query;

class DatabaseService {

  private $host;
  private $username;
  private $password;
  private $port;
  private $dbname;
  private $session;

  function __construct() {
    $this->host = 'localhost';
    $this->username = 'admin';
    $this->password = 'admin';
    $this->port = 1984;
    $this->dbname = 'Nuremberg';

    // Creating database
    $this->session = new Session($this->host, $this->port, $this->username, $this->password);
  }

  public function executeQuery($queryString) {
    try {
      $this->session->execute("OPEN ".$this->dbname);
      $queryStmt = $this->session->query($queryString);
      $result = $queryStmt->execute()."\n";
      $queryStmt->close();
      return $result;
    } catch (Exception $e) {
        print $e->getMessage();
    }
  }

  public function getTitles() {
    return "
        for \$title in //*:title
        let \$path := db:path(\$title)
        where \$title//text() != \"\"
        return <result><title>{\$title//text()}</title><path>{\$path}</path></result>
      ";
  }

  public function getDocument($path) {
    $path = $path.".xml";
    return "
      for \$sp in db:open('Nuremberg', \"$path\")//*:sp
      let \$speaker := \$sp//*:speaker/text()
      let \$p := \$sp//*:p/text()
      let \$doc := db:path(\$sp)
      return <res><sp>{\$speaker}</sp><text>{\$p}</text><path>{\$doc}</path></res>
    ";
  }

  public function getDocumentDetailBySpeaker($path, $term) {
    return "
      for \$sp in db:open('Nuremberg',  \"$path\")//*:sp
      where \$sp//text() = \"$term\"
      let \$speaker := \$sp//*:speaker/text()
      let \$p := \$sp//*:p/text()
      return <res><title>{db:open('Nuremberg', db:path(\$sp))//*:title/text()}</title>
      <sp>{\$speaker}</sp><text>{\$p}</text></res>
    ";
  }

  public function getDocumentDetail($path, $terms, $all) {

    $term_str = '{';
    foreach($terms as $index => $term) {
      $term_str .= "'".$term."'";
      if($index != sizeof($terms)-1) {
        $term_str .= ',';
      }
    }
    $term_str .= '}';

    return "
      for \$sp in db:open('Nuremberg', \"$path\")//*:sp
      where \$sp/*:p[text() contains text $term_str $all
      using stemming using language 'French']
      and \$sp/*:p/text() != ''
      let \$speaker := \$sp//*:speaker/text()
      let \$p := \$sp//*:p/text()
      return <res><title>{db:open('Nuremberg', db:path(\$sp))//*:title/text()}</title>
      <sp>{\$speaker}</sp><text>{\$p}</text></res>
    ";
  }

  public function getTitle($path) {
    return "
      for \$t in db:open('Nuremberg', \"$path\")//*:title
      let \$title := \$t/text()
      return \$title
    ";
  }

  public function getWithStemming($terms, $all) {
    /* building string for terms */

    $term_str = '{';
    foreach($terms as $index => $term) {
      $term_str .= "'".$term."'";
      if($index != sizeof($terms)-1) {
        $term_str .= ',';
      }
    }
    $term_str .= '}';

    return "
      for \$sp in //*:sp
      where \$sp/*:p[text() contains text $term_str $all
      using stemming using language 'French']
      and \$sp/*:p/text() != ''
      order by db:path(\$sp)
      return <resultat>
      <title>{db:open('Nuremberg', db:path(\$sp))//*:title/text()}</title>
      <speaker>{\$sp/*:speaker/text()}</speaker>
      <document>{db:path(\$sp)}</document>
      <paragraph>{\$sp//*:p/text()}</paragraph>
      </resultat>
    ";
  }

  public function getBySpeaker($speaker) {
    return "
      for \$sp in //*:sp
      where \$sp//text() = '$speaker'
      order by db:path(\$sp)
      return <resultat>
      <title>{db:open('Nuremberg', db:path(\$sp))//*:title/text()}</title>
      <speaker>{\$sp/*:speaker/text()}</speaker>
      <document>{db:path(\$sp)}</document>
      <paragraph>{\$sp//*:p/text()}</paragraph>
      </resultat>
    ";
  }

  public function getAllSpeakers() {
    return "
      let \$speakers := //*:sp/*:speaker/text()
      let \$unique-items := distinct-values(\$speakers)
      for \$s in \$unique-items
      return
      <speaker>
        {
          \$s
        }
      </speaker>
    ";
  }

  public function getSpeakers() {
    return "
      for \$i in //*:sp
      let \$speaker := \$i/*:speaker
      let \$speaker_name := \$speaker/text()
      return <span>{\$speaker_name}</span>
     ";
  }
}

/*
for $sp in //*:sp
let $speaker := $sp//*:speaker/text()
let $p := $sp//*:p/text()
let $doc := db:path($sp)
where db:open('Nuremberg', db:path($sp))//*:title/text() = "Deuxième journée"
return <res><sp>{$speaker}</sp><text>{$p}</text><path>{$doc}</path></res>
*/
?>
