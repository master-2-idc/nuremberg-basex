<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\DatabaseService;
use Symfony\Component\HttpFoundation\Request;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index(): Response
    {
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/advanced", name="advanced")
     */
    public function advanced(): Response
    {
        return $this->render('main/advanced.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/detail/{day}", name="detail", requirements={"day"=".+"})
     */
    public function detail($day, Request $request, DatabaseService $db): Response
    {
        $mode = $request->query->get('mode');
        $term = $request->query->get('term');
        $all = $request->query->get('all');

        if($mode == 'term') {
          $terms_array = explode(",", $term);
          $data = $db->executeQuery($db->getDocumentDetail($day, $terms_array, $all));
        } else if($mode == 'speaker') {
          $data = $db->executeQuery($db->getDocumentDetailBySpeaker($day,$term));
        }

        $title = $db->executeQuery($db->getTitle($day));

        $xml = <<< XML
        <results>
          $data
        </results>
        XML;

        $dom = new \DOMDocument;
        $dom->loadXML($xml, LIBXML_PARSEHUGE);
        $results = $dom->getElementsByTagName('res');
        $speakers = $dom->getElementsByTagName('sp');
        $paragraphs = $dom->getElementsByTagName('text');

        $res_array = array();
        /* associative array */
        foreach ($results as $index => $res) {
            $res_array[] = array('sp' => $speakers[$index]->nodeValue, 'text' => $paragraphs[$index]->nodeValue);
        }

        return $this->render('main/detail.html.twig', [
            'term' => $term,
            'day' => $title,
            'results' => $res_array
        ]);
    }


    /**
     * @Route("/api/titles", name="titles")
     */
     public function titles(DatabaseService $db): Response
     {
       $data = $db->executeQuery($db->getTitles());

       $xml = <<< XML
       <results>
         $data
       </results>
       XML;

       $dom = new \DOMDocument;
       $dom->loadXML($xml, LIBXML_PARSEHUGE);
       $titles = $dom->getElementsByTagName('title');

       $paths = $dom->getElementsByTagName('path');

       $arrayTitles = array();
       foreach ($titles as $title) {
          array_push($arrayTitles, $title->nodeValue);
      }

      $arrayPath = array();
      foreach ($paths as $path) {
         array_push($arrayPath, $path->nodeValue);
     }

       return $this->json([
         'titles' => $arrayTitles,
         'paths' => $arrayPath
       ]);
     }

     /**
      * @Route("/api/search/", name="search")
      */
      public function search(DatabaseService $db, Request $request): Response
      {
        $mode = $request->query->get('mode');
        $term = $request->query->get('term');
        $all  = $request->query->get('all');

        if($mode == 'term') {
          $terms_array = explode(",", $term);
          $data = $db->executeQuery($db->getWithStemming($terms_array, $all));
        } else if($mode == 'speaker') {
          $data = $db->executeQuery($db->getBySpeaker($term));
        }

        $xml = <<< XML
        <results>
          $data
        </results>
        XML;

        $dom = new \DOMDocument;
        $dom->loadXML($xml, LIBXML_PARSEHUGE);

        $results  = $dom->getElementsByTagName('resultat');
        $document = $dom->getElementsByTagName('document');
        $title    = $dom->getElementsByTagName('title');
        $results_len = $results->length;
        $countTest = 0;

        $array = array();
        $previous_document = "";
        $previous_index = 0;
        $number = 0;

        /* associative array */
        foreach ($document as $index => $doc) {
          $document_name = $doc->nodeValue;
          if($previous_document != $document_name) {
            // doc changed or is not initialized
            if($previous_document != "") {
              $array[] = array('document' => $previous_document, 'number' => $number, 'title' => $title[$previous_index]->nodeValue);
              $number = 1;
            }
            $previous_document = $document_name;
            $previous_index = $index;
            $number = 1;
          } else if($previous_document == $document_name) {
            $number += 1;
          }

          if($index == sizeof($document)-1) {
            $array[] = array('document' => $document_name, 'number' => $number, 'title' => $title[$index]->nodeValue);
          }
        }

        return $this->json([
          'terms' => $term,
          'data' => $array,
          'result_number' => $results_len,
          'documents_len' => count($array)
        ]);
      }

      /**
       * @Route("/api/speakers", name="speakers")
       */
       public function speakers(DatabaseService $db): Response
       {
         $data = $db->executeQuery($db->getAllSpeakers());

         $xml = <<< XML
         <results>
           $data
         </results>
         XML;

         $dom = new \DOMDocument;
         $dom->loadXML($xml, LIBXML_PARSEHUGE);
         $speakers = $dom->getElementsByTagName('speaker');

         $arraySp = array();
         foreach ($speakers as $speaker) {
          array_push($arraySp, $speaker->nodeValue);
        }

         return $this->json([
           'speakers' => $arraySp,
         ]);
       }

     /**
      * @Route("/api/document/{doc}", name="documents", requirements={"doc"=".+"})
      */
      public function documents(string $doc, DatabaseService $db): Response
      {
        $data = $db->executeQuery($db->getDocument($doc));

        $xml = <<< XML
        <results>
          $data
        </results>
        XML;

        $dom = new \DOMDocument;
        $dom->loadXML($xml, LIBXML_PARSEHUGE);
        $speakers = $dom->getElementsByTagName('sp');
        $pars = $dom->getElementsByTagName('text');

        $arraySp = array();
        foreach ($speakers as $speaker) {
           array_push($arraySp, $speaker->nodeValue);
         }

       $arrayPar = array();
       foreach ($pars as $par) {
          array_push($arrayPar, $par->nodeValue);
        }

        return $this->json([
          'sp' => $arraySp,
          'text' => $arrayPar
        ]);
      }

}
