import $ from 'jquery';

import '../styles/home.css';

let data_cache = { documents : []}
let $speakers_select = $("#speaker-select")
let $textContent     = $("#text-content")
let $search          = $("#search-btn")
let $select          = $("#day-select")
let $word_search     = $("#word-search")
let $result_number   = $("#result-number")
let $previous_button = $("#previous-button")
let $next_button     = $("#next-button")
let $day_title       = $("#day-title")
let $loader          = $("#loader")
let day_index        = 0
let timeout          = null

$select.on("change", function() {
  let speakers = []
  day_index = $(this).prop('selectedIndex');
  $textContent.html("")
  $day_title.html("")
  $loader.css("display", "block")
  $speakers_select.html("")
  displayNavigationButton(day_index)

  getArticle($select.val()).then(function(data) {
    $day_title.text($("#day-select option:selected").text())
    /* cache the result */
    data_cache.documents.push({title: $select.val(), d: data})
    $loader.css("display", "none")
    data.sp.forEach((item, i) => {
      if(!speakers.includes(item)) {
        speakers.push(item)
        $speakers_select.append(`<option>${item}</option>`)
      }
      $textContent.append(`<h5>${item}</h5><p>${data.text[i]}</p>`)
    });
  }).catch(console.error)
})

$word_search.on("input", function() {
  if(timeout !== null) {
    clearTimeout(timeout)
  }
  timeout = setTimeout(() => {
    let occurences = 0
    let value      = $(this).val()
    let paragraphs = $textContent.find("p")
    let speakers   = $textContent.find("h5")

    paragraphs.each(function(index) {
      let text = $(this).text().toLowerCase()
      if(!text.includes(value.toLowerCase())) {
        $(this).css("display", "none")
        $(speakers[index]).css("display", "none")
      } else {
        $(this).css("display", "block")
        $(speakers[index]).css("display", "block")
        /* highlight in text */
        let reg = new RegExp('('+value+')', 'gi');
        occurences += ($(this).text().match(reg) || []).length
        let new_text = $(this).text().replace(reg, `<span class="search-item">$1</span>`)
        $(this).html(new_text)
      }
    })

    if(value !== "" && occurences != 0) {
      $result_number.text(`Le terme apparaît ${occurences} fois`)
    } else if(occurences == 0){
      $result_number.text(`Aucun résultat pour ce terme`)
    } else {
      $result_number.text('')
    }
  }, 200);
})

$speakers_select.on("change", function() {
  let value = $select.val();
  let $current_speaker = $(this).val()
  $textContent.html("")

  /* load text in cache */
  let doc_filtered = data_cache.documents.filter(function(doc) {
    return doc.title === value
  })

  doc_filtered[0].d.sp.forEach((item, i) => {
    if(item == $current_speaker) {
      $textContent.append(`<h5>${item}</h5><p>${doc_filtered[0].d.text[i]}</p>`)
    }
  });
})

$previous_button.on("click", function() {
  day_index -= 1
  displayNavigationButton(day_index)
  loadTextFromIndex(day_index)
  let $option = $select.find(`option:nth-child(${day_index+1})`)
  $option.prop('selected', true)
})

$next_button.on("click", function() {
  day_index += 1
  displayNavigationButton(day_index)
  loadTextFromIndex(day_index)
  let $option = $select.find(`option:nth-child(${day_index+1})`)
  $option.prop('selected', true)
})

function loadTextFromIndex(index) {
  let speakers = []
  $textContent.html("")
  $speakers_select.html("")
  $day_title.html("")
  $loader.css("display", "block")
  let $option = $select.find(`option:nth-child(${day_index+1})`)
  let value  = $option.val()
  let text = $option.text()
  getArticle(value).then(function(data) {
    $loader.css("display", "none")
    $textContent.text("")
    $day_title.text(text)
    data.sp.forEach((item, i) => {
      if(!speakers.includes(item)) {
        speakers.push(item)
        $speakers_select.append(`<option>${item}</option>`)
      }
      $textContent.append(`<h5>${item}</h5><p>${data.text[i]}</p>`)
    });
  }).catch(console.error)
}

/* Functions */
function getTitles() {
  return new Promise((resolve, reject) => {
    $.ajax({
       url : '/api/titles',
       type : 'GET',
       success : function(res){
         resolve(res)
       },
       error : function(res, status, err){
         reject(err)
       }
    });
  })
}

function getArticle(title) {
  title = title.replace(".xml", "")
  return new Promise((resolve, reject) => {
    $.ajax({
       url : `/api/document/${title}`,
       type : 'GET',
       success : function(res){
         resolve(res)
       },
       error : function(res, status, err){
         reject(err)
       }
    });
  })
}

function displayNavigationButton(index) {
  if(index > 1 && index < 221) {
    $next_button.removeAttr("disabled")
    $previous_button.removeAttr("disabled")
  } else if(index > 1) {
    $next_button.attr("disabled", "disabled")
    $previous_button.removeAttr("disabled")
  } else {
    $next_button.removeAttr("disabled")
    $previous_button.attr("disabled", "disabled")
  }
}

getTitles().then((opt) => {
  opt.titles.forEach((item, i) => {
    $select.append(`<option value="${opt.paths[i]}">${item}</option>`)
  });
}).catch(console.error)
