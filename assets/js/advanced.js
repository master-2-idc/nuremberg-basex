import $ from 'jquery';

import '../styles/advanced.css';

const $search_input = $("#search-input")
const $search       = $("#search-btn")
const $result_ctn   = $("#results-container")
const $loader       = $("#loader")
const $speaker_select = $("#speaker-select")
const $result_number_ctn = $(".result-number-container")
const $mode_select = $("#mode-select")
const $all_any_select = $("#all-any-select")

let mode = 'term'

/* loading all speakers for searching by speakers */
getAllSpeakers().then(function(response) {
  let speakers = response.speakers
  speakers.forEach(speaker => {
    let $option = $("<option>").attr("value", speaker).text(speaker)
    $speaker_select.append($option)
  });
}).catch(console.error)

$mode_select.on('change', function() {
  let selected = $(this).val()
  if(selected == 'term')
  {
    mode = 'term'
    $speaker_select.css('display', 'none')
    $search_input.css('display', 'block')
    $all_any_select.css('display', 'block')
  }
  else if(selected == 'speaker')
  {
    mode = 'speaker'
    $speaker_select.css('display', 'block')
    $search_input.css('display', 'none')
    $all_any_select.css('display', 'none')
  }
})

/* Event listeners */
$search.on("click", function(e) {
  e.preventDefault()
  let input_value = ''
  mode == 'term' ? input_value = $search_input.val() : input_value = $speaker_select.val()
  let all = $all_any_select.val()
  $result_ctn.html("")
  $loader.css("display", "block")
  getWithStemming(input_value, mode, all).then(function(res) {

    $loader.css("display", "none")
    res.data.forEach(item => {
      let html =
      `<div class="card">
          <div class="card-body">
            <h5 class="card-title"><a href="detail/${item.document}?mode=${mode}&term=${input_value}&all=${all}">${item.title}</a> (${item.document})</h5>
            <p class="card-text">${item.number} paragraphes correspondant.</p>
          </div>
        </div>`
        $result_ctn.append(html)
    })

    $result_number_ctn.text(`${res.result_number} résultats trouvés dans ${res.documents_len} documents`)
  })
})

function getWithStemming(terms, mode, all) {
  return new Promise((resolve, reject) => {
    $.ajax({
       url : `/api/search/?mode=${mode}&term=${terms}&all=${all}`,
       type : 'GET',
       success : function(res){
         resolve(res)
       },
       error : function(res, status, err){
         reject(err)
       }
    });
  })
}

function getAllSpeakers() {
  return new Promise((resolve, reject) => {
    $.ajax({
       url : `/api/speakers`,
       type : 'GET',
       success : function(res){
         resolve(res)
       },
       error : function(res, status, err){
         reject(err)
       }
    });
  })
}
