import $ from 'jquery';

import '../styles/detail.css';

const urlParams = new URLSearchParams(window.location.href);
const term = urlParams.get('term').split(",");

let $text_container = $(".text-container")
let $paragraphs     = $text_container.find("p")

term.forEach(term => {
  let reg = new RegExp('('+term+')', 'gi')
  $paragraphs.each(function(index) {
    let p = $(this).html()
    let new_text = p.replace(reg, `<span class="search-item">$1</span>`)
    $(this).html(new_text)
  })
});
