(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home"],{

/***/ "./assets/js/home.js":
/*!***************************!*\
  !*** ./assets/js/home.js ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.filter */ "./node_modules/core-js/modules/es.array.filter.js");
/* harmony import */ var core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");
/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_includes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.includes */ "./node_modules/core-js/modules/es.array.includes.js");
/* harmony import */ var core_js_modules_es_array_includes__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_includes__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_regexp_constructor__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.regexp.constructor */ "./node_modules/core-js/modules/es.regexp.constructor.js");
/* harmony import */ var core_js_modules_es_regexp_constructor__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_constructor__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_includes__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.includes */ "./node_modules/core-js/modules/es.string.includes.js");
/* harmony import */ var core_js_modules_es_string_includes__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_includes__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.string.match */ "./node_modules/core-js/modules/es.string.match.js");
/* harmony import */ var core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/web.timers */ "./node_modules/core-js/modules/web.timers.js");
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _styles_home_css__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../styles/home.css */ "./assets/styles/home.css");
/* harmony import */ var _styles_home_css__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_styles_home_css__WEBPACK_IMPORTED_MODULE_16__);

















var data_cache = {
  documents: []
};
var $speakers_select = jquery__WEBPACK_IMPORTED_MODULE_15___default()("#speaker-select");
var $textContent = jquery__WEBPACK_IMPORTED_MODULE_15___default()("#text-content");
var $search = jquery__WEBPACK_IMPORTED_MODULE_15___default()("#search-btn");
var $select = jquery__WEBPACK_IMPORTED_MODULE_15___default()("#day-select");
var $word_search = jquery__WEBPACK_IMPORTED_MODULE_15___default()("#word-search");
var $result_number = jquery__WEBPACK_IMPORTED_MODULE_15___default()("#result-number");
var $previous_button = jquery__WEBPACK_IMPORTED_MODULE_15___default()("#previous-button");
var $next_button = jquery__WEBPACK_IMPORTED_MODULE_15___default()("#next-button");
var $day_title = jquery__WEBPACK_IMPORTED_MODULE_15___default()("#day-title");
var $loader = jquery__WEBPACK_IMPORTED_MODULE_15___default()("#loader");
var day_index = 0;
var timeout = null;
$select.on("change", function () {
  var speakers = [];
  day_index = jquery__WEBPACK_IMPORTED_MODULE_15___default()(this).prop('selectedIndex');
  $textContent.html("");
  $day_title.html("");
  $loader.css("display", "block");
  $speakers_select.html("");
  displayNavigationButton(day_index);
  getArticle($select.val()).then(function (data) {
    $day_title.text(jquery__WEBPACK_IMPORTED_MODULE_15___default()("#day-select option:selected").text());
    /* cache the result */

    data_cache.documents.push({
      title: $select.val(),
      d: data
    });
    $loader.css("display", "none");
    data.sp.forEach(function (item, i) {
      if (!speakers.includes(item)) {
        speakers.push(item);
        $speakers_select.append("<option>".concat(item, "</option>"));
      }

      $textContent.append("<h5>".concat(item, "</h5><p>").concat(data.text[i], "</p>"));
    });
  })["catch"](console.error);
});
$word_search.on("input", function () {
  var _this = this;

  if (timeout !== null) {
    clearTimeout(timeout);
  }

  timeout = setTimeout(function () {
    var occurences = 0;
    var value = jquery__WEBPACK_IMPORTED_MODULE_15___default()(_this).val();
    var paragraphs = $textContent.find("p");
    var speakers = $textContent.find("h5");
    paragraphs.each(function (index) {
      var text = jquery__WEBPACK_IMPORTED_MODULE_15___default()(this).text().toLowerCase();

      if (!text.includes(value.toLowerCase())) {
        jquery__WEBPACK_IMPORTED_MODULE_15___default()(this).css("display", "none");
        jquery__WEBPACK_IMPORTED_MODULE_15___default()(speakers[index]).css("display", "none");
      } else {
        jquery__WEBPACK_IMPORTED_MODULE_15___default()(this).css("display", "block");
        jquery__WEBPACK_IMPORTED_MODULE_15___default()(speakers[index]).css("display", "block");
        /* highlight in text */

        var reg = new RegExp('(' + value + ')', 'gi');
        occurences += (jquery__WEBPACK_IMPORTED_MODULE_15___default()(this).text().match(reg) || []).length;
        var new_text = jquery__WEBPACK_IMPORTED_MODULE_15___default()(this).text().replace(reg, "<span class=\"search-item\">$1</span>");
        jquery__WEBPACK_IMPORTED_MODULE_15___default()(this).html(new_text);
      }
    });

    if (value !== "" && occurences != 0) {
      $result_number.text("Le terme appara\xEEt ".concat(occurences, " fois"));
    } else if (occurences == 0) {
      $result_number.text("Aucun r\xE9sultat pour ce terme");
    } else {
      $result_number.text('');
    }
  }, 200);
});
$speakers_select.on("change", function () {
  var value = $select.val();
  var $current_speaker = jquery__WEBPACK_IMPORTED_MODULE_15___default()(this).val();
  $textContent.html("");
  /* load text in cache */

  var doc_filtered = data_cache.documents.filter(function (doc) {
    return doc.title === value;
  });
  doc_filtered[0].d.sp.forEach(function (item, i) {
    if (item == $current_speaker) {
      $textContent.append("<h5>".concat(item, "</h5><p>").concat(doc_filtered[0].d.text[i], "</p>"));
    }
  });
});
$previous_button.on("click", function () {
  day_index -= 1;
  displayNavigationButton(day_index);
  loadTextFromIndex(day_index);
  var $option = $select.find("option:nth-child(".concat(day_index + 1, ")"));
  $option.prop('selected', true);
});
$next_button.on("click", function () {
  day_index += 1;
  displayNavigationButton(day_index);
  loadTextFromIndex(day_index);
  var $option = $select.find("option:nth-child(".concat(day_index + 1, ")"));
  $option.prop('selected', true);
});

function loadTextFromIndex(index) {
  var speakers = [];
  $textContent.html("");
  $speakers_select.html("");
  $day_title.html("");
  $loader.css("display", "block");
  var $option = $select.find("option:nth-child(".concat(day_index + 1, ")"));
  var value = $option.val();
  var text = $option.text();
  getArticle(value).then(function (data) {
    $loader.css("display", "none");
    $textContent.text("");
    $day_title.text(text);
    data.sp.forEach(function (item, i) {
      if (!speakers.includes(item)) {
        speakers.push(item);
        $speakers_select.append("<option>".concat(item, "</option>"));
      }

      $textContent.append("<h5>".concat(item, "</h5><p>").concat(data.text[i], "</p>"));
    });
  })["catch"](console.error);
}
/* Functions */


function getTitles() {
  return new Promise(function (resolve, reject) {
    jquery__WEBPACK_IMPORTED_MODULE_15___default.a.ajax({
      url: '/api/titles',
      type: 'GET',
      success: function success(res) {
        resolve(res);
      },
      error: function error(res, status, err) {
        reject(err);
      }
    });
  });
}

function getArticle(title) {
  title = title.replace(".xml", "");
  return new Promise(function (resolve, reject) {
    jquery__WEBPACK_IMPORTED_MODULE_15___default.a.ajax({
      url: "/api/document/".concat(title),
      type: 'GET',
      success: function success(res) {
        resolve(res);
      },
      error: function error(res, status, err) {
        reject(err);
      }
    });
  });
}

function displayNavigationButton(index) {
  if (index > 1 && index < 221) {
    $next_button.removeAttr("disabled");
    $previous_button.removeAttr("disabled");
  } else if (index > 1) {
    $next_button.attr("disabled", "disabled");
    $previous_button.removeAttr("disabled");
  } else {
    $next_button.removeAttr("disabled");
    $previous_button.attr("disabled", "disabled");
  }
}

getTitles().then(function (opt) {
  opt.titles.forEach(function (item, i) {
    $select.append("<option value=\"".concat(opt.paths[i], "\">").concat(item, "</option>"));
  });
})["catch"](console.error);

/***/ }),

/***/ "./assets/styles/home.css":
/*!********************************!*\
  !*** ./assets/styles/home.css ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./node_modules/core-js/internals/correct-is-regexp-logic.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/internals/correct-is-regexp-logic.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var wellKnownSymbol = __webpack_require__(/*! ../internals/well-known-symbol */ "./node_modules/core-js/internals/well-known-symbol.js");

var MATCH = wellKnownSymbol('match');

module.exports = function (METHOD_NAME) {
  var regexp = /./;
  try {
    '/./'[METHOD_NAME](regexp);
  } catch (error1) {
    try {
      regexp[MATCH] = false;
      return '/./'[METHOD_NAME](regexp);
    } catch (error2) { /* empty */ }
  } return false;
};


/***/ }),

/***/ "./node_modules/core-js/internals/not-a-regexp.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/internals/not-a-regexp.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isRegExp = __webpack_require__(/*! ../internals/is-regexp */ "./node_modules/core-js/internals/is-regexp.js");

module.exports = function (it) {
  if (isRegExp(it)) {
    throw TypeError("The method doesn't accept regular expressions");
  } return it;
};


/***/ }),

/***/ "./node_modules/core-js/modules/es.array.filter.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/modules/es.array.filter.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $ = __webpack_require__(/*! ../internals/export */ "./node_modules/core-js/internals/export.js");
var $filter = __webpack_require__(/*! ../internals/array-iteration */ "./node_modules/core-js/internals/array-iteration.js").filter;
var arrayMethodHasSpeciesSupport = __webpack_require__(/*! ../internals/array-method-has-species-support */ "./node_modules/core-js/internals/array-method-has-species-support.js");
var arrayMethodUsesToLength = __webpack_require__(/*! ../internals/array-method-uses-to-length */ "./node_modules/core-js/internals/array-method-uses-to-length.js");

var HAS_SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('filter');
// Edge 14- issue
var USES_TO_LENGTH = arrayMethodUsesToLength('filter');

// `Array.prototype.filter` method
// https://tc39.github.io/ecma262/#sec-array.prototype.filter
// with adding support of @@species
$({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT || !USES_TO_LENGTH }, {
  filter: function filter(callbackfn /* , thisArg */) {
    return $filter(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  }
});


/***/ }),

/***/ "./node_modules/core-js/modules/es.array.includes.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/modules/es.array.includes.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $ = __webpack_require__(/*! ../internals/export */ "./node_modules/core-js/internals/export.js");
var $includes = __webpack_require__(/*! ../internals/array-includes */ "./node_modules/core-js/internals/array-includes.js").includes;
var addToUnscopables = __webpack_require__(/*! ../internals/add-to-unscopables */ "./node_modules/core-js/internals/add-to-unscopables.js");
var arrayMethodUsesToLength = __webpack_require__(/*! ../internals/array-method-uses-to-length */ "./node_modules/core-js/internals/array-method-uses-to-length.js");

var USES_TO_LENGTH = arrayMethodUsesToLength('indexOf', { ACCESSORS: true, 1: 0 });

// `Array.prototype.includes` method
// https://tc39.github.io/ecma262/#sec-array.prototype.includes
$({ target: 'Array', proto: true, forced: !USES_TO_LENGTH }, {
  includes: function includes(el /* , fromIndex = 0 */) {
    return $includes(this, el, arguments.length > 1 ? arguments[1] : undefined);
  }
});

// https://tc39.github.io/ecma262/#sec-array.prototype-@@unscopables
addToUnscopables('includes');


/***/ }),

/***/ "./node_modules/core-js/modules/es.string.includes.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/modules/es.string.includes.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $ = __webpack_require__(/*! ../internals/export */ "./node_modules/core-js/internals/export.js");
var notARegExp = __webpack_require__(/*! ../internals/not-a-regexp */ "./node_modules/core-js/internals/not-a-regexp.js");
var requireObjectCoercible = __webpack_require__(/*! ../internals/require-object-coercible */ "./node_modules/core-js/internals/require-object-coercible.js");
var correctIsRegExpLogic = __webpack_require__(/*! ../internals/correct-is-regexp-logic */ "./node_modules/core-js/internals/correct-is-regexp-logic.js");

// `String.prototype.includes` method
// https://tc39.github.io/ecma262/#sec-string.prototype.includes
$({ target: 'String', proto: true, forced: !correctIsRegExpLogic('includes') }, {
  includes: function includes(searchString /* , position = 0 */) {
    return !!~String(requireObjectCoercible(this))
      .indexOf(notARegExp(searchString), arguments.length > 1 ? arguments[1] : undefined);
  }
});


/***/ }),

/***/ "./node_modules/core-js/modules/es.string.match.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/modules/es.string.match.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var fixRegExpWellKnownSymbolLogic = __webpack_require__(/*! ../internals/fix-regexp-well-known-symbol-logic */ "./node_modules/core-js/internals/fix-regexp-well-known-symbol-logic.js");
var anObject = __webpack_require__(/*! ../internals/an-object */ "./node_modules/core-js/internals/an-object.js");
var toLength = __webpack_require__(/*! ../internals/to-length */ "./node_modules/core-js/internals/to-length.js");
var requireObjectCoercible = __webpack_require__(/*! ../internals/require-object-coercible */ "./node_modules/core-js/internals/require-object-coercible.js");
var advanceStringIndex = __webpack_require__(/*! ../internals/advance-string-index */ "./node_modules/core-js/internals/advance-string-index.js");
var regExpExec = __webpack_require__(/*! ../internals/regexp-exec-abstract */ "./node_modules/core-js/internals/regexp-exec-abstract.js");

// @@match logic
fixRegExpWellKnownSymbolLogic('match', 1, function (MATCH, nativeMatch, maybeCallNative) {
  return [
    // `String.prototype.match` method
    // https://tc39.github.io/ecma262/#sec-string.prototype.match
    function match(regexp) {
      var O = requireObjectCoercible(this);
      var matcher = regexp == undefined ? undefined : regexp[MATCH];
      return matcher !== undefined ? matcher.call(regexp, O) : new RegExp(regexp)[MATCH](String(O));
    },
    // `RegExp.prototype[@@match]` method
    // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@match
    function (regexp) {
      var res = maybeCallNative(nativeMatch, regexp, this);
      if (res.done) return res.value;

      var rx = anObject(regexp);
      var S = String(this);

      if (!rx.global) return regExpExec(rx, S);

      var fullUnicode = rx.unicode;
      rx.lastIndex = 0;
      var A = [];
      var n = 0;
      var result;
      while ((result = regExpExec(rx, S)) !== null) {
        var matchStr = String(result[0]);
        A[n] = matchStr;
        if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
        n++;
      }
      return n === 0 ? null : A;
    }
  ];
});


/***/ }),

/***/ "./node_modules/core-js/modules/web.timers.js":
/*!****************************************************!*\
  !*** ./node_modules/core-js/modules/web.timers.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var $ = __webpack_require__(/*! ../internals/export */ "./node_modules/core-js/internals/export.js");
var global = __webpack_require__(/*! ../internals/global */ "./node_modules/core-js/internals/global.js");
var userAgent = __webpack_require__(/*! ../internals/engine-user-agent */ "./node_modules/core-js/internals/engine-user-agent.js");

var slice = [].slice;
var MSIE = /MSIE .\./.test(userAgent); // <- dirty ie9- check

var wrap = function (scheduler) {
  return function (handler, timeout /* , ...arguments */) {
    var boundArgs = arguments.length > 2;
    var args = boundArgs ? slice.call(arguments, 2) : undefined;
    return scheduler(boundArgs ? function () {
      // eslint-disable-next-line no-new-func
      (typeof handler == 'function' ? handler : Function(handler)).apply(this, args);
    } : handler, timeout);
  };
};

// ie9- setTimeout & setInterval additional parameters fix
// https://html.spec.whatwg.org/multipage/timers-and-user-prompts.html#timers
$({ global: true, bind: true, forced: MSIE }, {
  // `setTimeout` method
  // https://html.spec.whatwg.org/multipage/timers-and-user-prompts.html#dom-settimeout
  setTimeout: wrap(global.setTimeout),
  // `setInterval` method
  // https://html.spec.whatwg.org/multipage/timers-and-user-prompts.html#dom-setinterval
  setInterval: wrap(global.setInterval)
});


/***/ })

},[["./assets/js/home.js","runtime","vendors~advanced~app~detail~home","vendors~advanced~detail~home","vendors~detail~home","vendors~advanced~home"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvaG9tZS5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvc3R5bGVzL2hvbWUuY3NzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2ludGVybmFscy9jb3JyZWN0LWlzLXJlZ2V4cC1sb2dpYy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9pbnRlcm5hbHMvbm90LWEtcmVnZXhwLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL21vZHVsZXMvZXMuYXJyYXkuZmlsdGVyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL21vZHVsZXMvZXMuYXJyYXkuaW5jbHVkZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbW9kdWxlcy9lcy5zdHJpbmcuaW5jbHVkZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbW9kdWxlcy9lcy5zdHJpbmcubWF0Y2guanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbW9kdWxlcy93ZWIudGltZXJzLmpzIl0sIm5hbWVzIjpbImRhdGFfY2FjaGUiLCJkb2N1bWVudHMiLCIkc3BlYWtlcnNfc2VsZWN0IiwiJCIsIiR0ZXh0Q29udGVudCIsIiRzZWFyY2giLCIkc2VsZWN0IiwiJHdvcmRfc2VhcmNoIiwiJHJlc3VsdF9udW1iZXIiLCIkcHJldmlvdXNfYnV0dG9uIiwiJG5leHRfYnV0dG9uIiwiJGRheV90aXRsZSIsIiRsb2FkZXIiLCJkYXlfaW5kZXgiLCJ0aW1lb3V0Iiwib24iLCJzcGVha2VycyIsInByb3AiLCJodG1sIiwiY3NzIiwiZGlzcGxheU5hdmlnYXRpb25CdXR0b24iLCJnZXRBcnRpY2xlIiwidmFsIiwidGhlbiIsImRhdGEiLCJ0ZXh0IiwicHVzaCIsInRpdGxlIiwiZCIsInNwIiwiZm9yRWFjaCIsIml0ZW0iLCJpIiwiaW5jbHVkZXMiLCJhcHBlbmQiLCJjb25zb2xlIiwiZXJyb3IiLCJjbGVhclRpbWVvdXQiLCJzZXRUaW1lb3V0Iiwib2NjdXJlbmNlcyIsInZhbHVlIiwicGFyYWdyYXBocyIsImZpbmQiLCJlYWNoIiwiaW5kZXgiLCJ0b0xvd2VyQ2FzZSIsInJlZyIsIlJlZ0V4cCIsIm1hdGNoIiwibGVuZ3RoIiwibmV3X3RleHQiLCJyZXBsYWNlIiwiJGN1cnJlbnRfc3BlYWtlciIsImRvY19maWx0ZXJlZCIsImZpbHRlciIsImRvYyIsImxvYWRUZXh0RnJvbUluZGV4IiwiJG9wdGlvbiIsImdldFRpdGxlcyIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0IiwiYWpheCIsInVybCIsInR5cGUiLCJzdWNjZXNzIiwicmVzIiwic3RhdHVzIiwiZXJyIiwicmVtb3ZlQXR0ciIsImF0dHIiLCJvcHQiLCJ0aXRsZXMiLCJwYXRocyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFFQTtBQUVBLElBQUlBLFVBQVUsR0FBRztBQUFFQyxXQUFTLEVBQUc7QUFBZCxDQUFqQjtBQUNBLElBQUlDLGdCQUFnQixHQUFHQyw4Q0FBQyxDQUFDLGlCQUFELENBQXhCO0FBQ0EsSUFBSUMsWUFBWSxHQUFPRCw4Q0FBQyxDQUFDLGVBQUQsQ0FBeEI7QUFDQSxJQUFJRSxPQUFPLEdBQVlGLDhDQUFDLENBQUMsYUFBRCxDQUF4QjtBQUNBLElBQUlHLE9BQU8sR0FBWUgsOENBQUMsQ0FBQyxhQUFELENBQXhCO0FBQ0EsSUFBSUksWUFBWSxHQUFPSiw4Q0FBQyxDQUFDLGNBQUQsQ0FBeEI7QUFDQSxJQUFJSyxjQUFjLEdBQUtMLDhDQUFDLENBQUMsZ0JBQUQsQ0FBeEI7QUFDQSxJQUFJTSxnQkFBZ0IsR0FBR04sOENBQUMsQ0FBQyxrQkFBRCxDQUF4QjtBQUNBLElBQUlPLFlBQVksR0FBT1AsOENBQUMsQ0FBQyxjQUFELENBQXhCO0FBQ0EsSUFBSVEsVUFBVSxHQUFTUiw4Q0FBQyxDQUFDLFlBQUQsQ0FBeEI7QUFDQSxJQUFJUyxPQUFPLEdBQVlULDhDQUFDLENBQUMsU0FBRCxDQUF4QjtBQUNBLElBQUlVLFNBQVMsR0FBVSxDQUF2QjtBQUNBLElBQUlDLE9BQU8sR0FBWSxJQUF2QjtBQUVBUixPQUFPLENBQUNTLEVBQVIsQ0FBVyxRQUFYLEVBQXFCLFlBQVc7QUFDOUIsTUFBSUMsUUFBUSxHQUFHLEVBQWY7QUFDQUgsV0FBUyxHQUFHViw4Q0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRYyxJQUFSLENBQWEsZUFBYixDQUFaO0FBQ0FiLGNBQVksQ0FBQ2MsSUFBYixDQUFrQixFQUFsQjtBQUNBUCxZQUFVLENBQUNPLElBQVgsQ0FBZ0IsRUFBaEI7QUFDQU4sU0FBTyxDQUFDTyxHQUFSLENBQVksU0FBWixFQUF1QixPQUF2QjtBQUNBakIsa0JBQWdCLENBQUNnQixJQUFqQixDQUFzQixFQUF0QjtBQUNBRSx5QkFBdUIsQ0FBQ1AsU0FBRCxDQUF2QjtBQUVBUSxZQUFVLENBQUNmLE9BQU8sQ0FBQ2dCLEdBQVIsRUFBRCxDQUFWLENBQTBCQyxJQUExQixDQUErQixVQUFTQyxJQUFULEVBQWU7QUFDNUNiLGNBQVUsQ0FBQ2MsSUFBWCxDQUFnQnRCLDhDQUFDLENBQUMsNkJBQUQsQ0FBRCxDQUFpQ3NCLElBQWpDLEVBQWhCO0FBQ0E7O0FBQ0F6QixjQUFVLENBQUNDLFNBQVgsQ0FBcUJ5QixJQUFyQixDQUEwQjtBQUFDQyxXQUFLLEVBQUVyQixPQUFPLENBQUNnQixHQUFSLEVBQVI7QUFBdUJNLE9BQUMsRUFBRUo7QUFBMUIsS0FBMUI7QUFDQVosV0FBTyxDQUFDTyxHQUFSLENBQVksU0FBWixFQUF1QixNQUF2QjtBQUNBSyxRQUFJLENBQUNLLEVBQUwsQ0FBUUMsT0FBUixDQUFnQixVQUFDQyxJQUFELEVBQU9DLENBQVAsRUFBYTtBQUMzQixVQUFHLENBQUNoQixRQUFRLENBQUNpQixRQUFULENBQWtCRixJQUFsQixDQUFKLEVBQTZCO0FBQzNCZixnQkFBUSxDQUFDVSxJQUFULENBQWNLLElBQWQ7QUFDQTdCLHdCQUFnQixDQUFDZ0MsTUFBakIsbUJBQW1DSCxJQUFuQztBQUNEOztBQUNEM0Isa0JBQVksQ0FBQzhCLE1BQWIsZUFBMkJILElBQTNCLHFCQUEwQ1AsSUFBSSxDQUFDQyxJQUFMLENBQVVPLENBQVYsQ0FBMUM7QUFDRCxLQU5EO0FBT0QsR0FaRCxXQVlTRyxPQUFPLENBQUNDLEtBWmpCO0FBYUQsQ0F0QkQ7QUF3QkE3QixZQUFZLENBQUNRLEVBQWIsQ0FBZ0IsT0FBaEIsRUFBeUIsWUFBVztBQUFBOztBQUNsQyxNQUFHRCxPQUFPLEtBQUssSUFBZixFQUFxQjtBQUNuQnVCLGdCQUFZLENBQUN2QixPQUFELENBQVo7QUFDRDs7QUFDREEsU0FBTyxHQUFHd0IsVUFBVSxDQUFDLFlBQU07QUFDekIsUUFBSUMsVUFBVSxHQUFHLENBQWpCO0FBQ0EsUUFBSUMsS0FBSyxHQUFRckMsOENBQUMsQ0FBQyxLQUFELENBQUQsQ0FBUW1CLEdBQVIsRUFBakI7QUFDQSxRQUFJbUIsVUFBVSxHQUFHckMsWUFBWSxDQUFDc0MsSUFBYixDQUFrQixHQUFsQixDQUFqQjtBQUNBLFFBQUkxQixRQUFRLEdBQUtaLFlBQVksQ0FBQ3NDLElBQWIsQ0FBa0IsSUFBbEIsQ0FBakI7QUFFQUQsY0FBVSxDQUFDRSxJQUFYLENBQWdCLFVBQVNDLEtBQVQsRUFBZ0I7QUFDOUIsVUFBSW5CLElBQUksR0FBR3RCLDhDQUFDLENBQUMsSUFBRCxDQUFELENBQVFzQixJQUFSLEdBQWVvQixXQUFmLEVBQVg7O0FBQ0EsVUFBRyxDQUFDcEIsSUFBSSxDQUFDUSxRQUFMLENBQWNPLEtBQUssQ0FBQ0ssV0FBTixFQUFkLENBQUosRUFBd0M7QUFDdEMxQyxzREFBQyxDQUFDLElBQUQsQ0FBRCxDQUFRZ0IsR0FBUixDQUFZLFNBQVosRUFBdUIsTUFBdkI7QUFDQWhCLHNEQUFDLENBQUNhLFFBQVEsQ0FBQzRCLEtBQUQsQ0FBVCxDQUFELENBQW1CekIsR0FBbkIsQ0FBdUIsU0FBdkIsRUFBa0MsTUFBbEM7QUFDRCxPQUhELE1BR087QUFDTGhCLHNEQUFDLENBQUMsSUFBRCxDQUFELENBQVFnQixHQUFSLENBQVksU0FBWixFQUF1QixPQUF2QjtBQUNBaEIsc0RBQUMsQ0FBQ2EsUUFBUSxDQUFDNEIsS0FBRCxDQUFULENBQUQsQ0FBbUJ6QixHQUFuQixDQUF1QixTQUF2QixFQUFrQyxPQUFsQztBQUNBOztBQUNBLFlBQUkyQixHQUFHLEdBQUcsSUFBSUMsTUFBSixDQUFXLE1BQUlQLEtBQUosR0FBVSxHQUFyQixFQUEwQixJQUExQixDQUFWO0FBQ0FELGtCQUFVLElBQUksQ0FBQ3BDLDhDQUFDLENBQUMsSUFBRCxDQUFELENBQVFzQixJQUFSLEdBQWV1QixLQUFmLENBQXFCRixHQUFyQixLQUE2QixFQUE5QixFQUFrQ0csTUFBaEQ7QUFDQSxZQUFJQyxRQUFRLEdBQUcvQyw4Q0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRc0IsSUFBUixHQUFlMEIsT0FBZixDQUF1QkwsR0FBdkIsMENBQWY7QUFDQTNDLHNEQUFDLENBQUMsSUFBRCxDQUFELENBQVFlLElBQVIsQ0FBYWdDLFFBQWI7QUFDRDtBQUNGLEtBZEQ7O0FBZ0JBLFFBQUdWLEtBQUssS0FBSyxFQUFWLElBQWdCRCxVQUFVLElBQUksQ0FBakMsRUFBb0M7QUFDbEMvQixvQkFBYyxDQUFDaUIsSUFBZixnQ0FBeUNjLFVBQXpDO0FBQ0QsS0FGRCxNQUVPLElBQUdBLFVBQVUsSUFBSSxDQUFqQixFQUFtQjtBQUN4Qi9CLG9CQUFjLENBQUNpQixJQUFmO0FBQ0QsS0FGTSxNQUVBO0FBQ0xqQixvQkFBYyxDQUFDaUIsSUFBZixDQUFvQixFQUFwQjtBQUNEO0FBQ0YsR0E3Qm1CLEVBNkJqQixHQTdCaUIsQ0FBcEI7QUE4QkQsQ0FsQ0Q7QUFvQ0F2QixnQkFBZ0IsQ0FBQ2EsRUFBakIsQ0FBb0IsUUFBcEIsRUFBOEIsWUFBVztBQUN2QyxNQUFJeUIsS0FBSyxHQUFHbEMsT0FBTyxDQUFDZ0IsR0FBUixFQUFaO0FBQ0EsTUFBSThCLGdCQUFnQixHQUFHakQsOENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUW1CLEdBQVIsRUFBdkI7QUFDQWxCLGNBQVksQ0FBQ2MsSUFBYixDQUFrQixFQUFsQjtBQUVBOztBQUNBLE1BQUltQyxZQUFZLEdBQUdyRCxVQUFVLENBQUNDLFNBQVgsQ0FBcUJxRCxNQUFyQixDQUE0QixVQUFTQyxHQUFULEVBQWM7QUFDM0QsV0FBT0EsR0FBRyxDQUFDNUIsS0FBSixLQUFjYSxLQUFyQjtBQUNELEdBRmtCLENBQW5CO0FBSUFhLGNBQVksQ0FBQyxDQUFELENBQVosQ0FBZ0J6QixDQUFoQixDQUFrQkMsRUFBbEIsQ0FBcUJDLE9BQXJCLENBQTZCLFVBQUNDLElBQUQsRUFBT0MsQ0FBUCxFQUFhO0FBQ3hDLFFBQUdELElBQUksSUFBSXFCLGdCQUFYLEVBQTZCO0FBQzNCaEQsa0JBQVksQ0FBQzhCLE1BQWIsZUFBMkJILElBQTNCLHFCQUEwQ3NCLFlBQVksQ0FBQyxDQUFELENBQVosQ0FBZ0J6QixDQUFoQixDQUFrQkgsSUFBbEIsQ0FBdUJPLENBQXZCLENBQTFDO0FBQ0Q7QUFDRixHQUpEO0FBS0QsQ0FmRDtBQWlCQXZCLGdCQUFnQixDQUFDTSxFQUFqQixDQUFvQixPQUFwQixFQUE2QixZQUFXO0FBQ3RDRixXQUFTLElBQUksQ0FBYjtBQUNBTyx5QkFBdUIsQ0FBQ1AsU0FBRCxDQUF2QjtBQUNBMkMsbUJBQWlCLENBQUMzQyxTQUFELENBQWpCO0FBQ0EsTUFBSTRDLE9BQU8sR0FBR25ELE9BQU8sQ0FBQ29DLElBQVIsNEJBQWlDN0IsU0FBUyxHQUFDLENBQTNDLE9BQWQ7QUFDQTRDLFNBQU8sQ0FBQ3hDLElBQVIsQ0FBYSxVQUFiLEVBQXlCLElBQXpCO0FBQ0QsQ0FORDtBQVFBUCxZQUFZLENBQUNLLEVBQWIsQ0FBZ0IsT0FBaEIsRUFBeUIsWUFBVztBQUNsQ0YsV0FBUyxJQUFJLENBQWI7QUFDQU8seUJBQXVCLENBQUNQLFNBQUQsQ0FBdkI7QUFDQTJDLG1CQUFpQixDQUFDM0MsU0FBRCxDQUFqQjtBQUNBLE1BQUk0QyxPQUFPLEdBQUduRCxPQUFPLENBQUNvQyxJQUFSLDRCQUFpQzdCLFNBQVMsR0FBQyxDQUEzQyxPQUFkO0FBQ0E0QyxTQUFPLENBQUN4QyxJQUFSLENBQWEsVUFBYixFQUF5QixJQUF6QjtBQUNELENBTkQ7O0FBUUEsU0FBU3VDLGlCQUFULENBQTJCWixLQUEzQixFQUFrQztBQUNoQyxNQUFJNUIsUUFBUSxHQUFHLEVBQWY7QUFDQVosY0FBWSxDQUFDYyxJQUFiLENBQWtCLEVBQWxCO0FBQ0FoQixrQkFBZ0IsQ0FBQ2dCLElBQWpCLENBQXNCLEVBQXRCO0FBQ0FQLFlBQVUsQ0FBQ08sSUFBWCxDQUFnQixFQUFoQjtBQUNBTixTQUFPLENBQUNPLEdBQVIsQ0FBWSxTQUFaLEVBQXVCLE9BQXZCO0FBQ0EsTUFBSXNDLE9BQU8sR0FBR25ELE9BQU8sQ0FBQ29DLElBQVIsNEJBQWlDN0IsU0FBUyxHQUFDLENBQTNDLE9BQWQ7QUFDQSxNQUFJMkIsS0FBSyxHQUFJaUIsT0FBTyxDQUFDbkMsR0FBUixFQUFiO0FBQ0EsTUFBSUcsSUFBSSxHQUFHZ0MsT0FBTyxDQUFDaEMsSUFBUixFQUFYO0FBQ0FKLFlBQVUsQ0FBQ21CLEtBQUQsQ0FBVixDQUFrQmpCLElBQWxCLENBQXVCLFVBQVNDLElBQVQsRUFBZTtBQUNwQ1osV0FBTyxDQUFDTyxHQUFSLENBQVksU0FBWixFQUF1QixNQUF2QjtBQUNBZixnQkFBWSxDQUFDcUIsSUFBYixDQUFrQixFQUFsQjtBQUNBZCxjQUFVLENBQUNjLElBQVgsQ0FBZ0JBLElBQWhCO0FBQ0FELFFBQUksQ0FBQ0ssRUFBTCxDQUFRQyxPQUFSLENBQWdCLFVBQUNDLElBQUQsRUFBT0MsQ0FBUCxFQUFhO0FBQzNCLFVBQUcsQ0FBQ2hCLFFBQVEsQ0FBQ2lCLFFBQVQsQ0FBa0JGLElBQWxCLENBQUosRUFBNkI7QUFDM0JmLGdCQUFRLENBQUNVLElBQVQsQ0FBY0ssSUFBZDtBQUNBN0Isd0JBQWdCLENBQUNnQyxNQUFqQixtQkFBbUNILElBQW5DO0FBQ0Q7O0FBQ0QzQixrQkFBWSxDQUFDOEIsTUFBYixlQUEyQkgsSUFBM0IscUJBQTBDUCxJQUFJLENBQUNDLElBQUwsQ0FBVU8sQ0FBVixDQUExQztBQUNELEtBTkQ7QUFPRCxHQVhELFdBV1NHLE9BQU8sQ0FBQ0MsS0FYakI7QUFZRDtBQUVEOzs7QUFDQSxTQUFTc0IsU0FBVCxHQUFxQjtBQUNuQixTQUFPLElBQUlDLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFDdEMxRCxrREFBQyxDQUFDMkQsSUFBRixDQUFPO0FBQ0pDLFNBQUcsRUFBRyxhQURGO0FBRUpDLFVBQUksRUFBRyxLQUZIO0FBR0pDLGFBQU8sRUFBRyxpQkFBU0MsR0FBVCxFQUFhO0FBQ3JCTixlQUFPLENBQUNNLEdBQUQsQ0FBUDtBQUNELE9BTEc7QUFNSjlCLFdBQUssRUFBRyxlQUFTOEIsR0FBVCxFQUFjQyxNQUFkLEVBQXNCQyxHQUF0QixFQUEwQjtBQUNoQ1AsY0FBTSxDQUFDTyxHQUFELENBQU47QUFDRDtBQVJHLEtBQVA7QUFVRCxHQVhNLENBQVA7QUFZRDs7QUFFRCxTQUFTL0MsVUFBVCxDQUFvQk0sS0FBcEIsRUFBMkI7QUFDekJBLE9BQUssR0FBR0EsS0FBSyxDQUFDd0IsT0FBTixDQUFjLE1BQWQsRUFBc0IsRUFBdEIsQ0FBUjtBQUNBLFNBQU8sSUFBSVEsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUN0QzFELGtEQUFDLENBQUMyRCxJQUFGLENBQU87QUFDSkMsU0FBRywwQkFBb0JwQyxLQUFwQixDQURDO0FBRUpxQyxVQUFJLEVBQUcsS0FGSDtBQUdKQyxhQUFPLEVBQUcsaUJBQVNDLEdBQVQsRUFBYTtBQUNyQk4sZUFBTyxDQUFDTSxHQUFELENBQVA7QUFDRCxPQUxHO0FBTUo5QixXQUFLLEVBQUcsZUFBUzhCLEdBQVQsRUFBY0MsTUFBZCxFQUFzQkMsR0FBdEIsRUFBMEI7QUFDaENQLGNBQU0sQ0FBQ08sR0FBRCxDQUFOO0FBQ0Q7QUFSRyxLQUFQO0FBVUQsR0FYTSxDQUFQO0FBWUQ7O0FBRUQsU0FBU2hELHVCQUFULENBQWlDd0IsS0FBakMsRUFBd0M7QUFDdEMsTUFBR0EsS0FBSyxHQUFHLENBQVIsSUFBYUEsS0FBSyxHQUFHLEdBQXhCLEVBQTZCO0FBQzNCbEMsZ0JBQVksQ0FBQzJELFVBQWIsQ0FBd0IsVUFBeEI7QUFDQTVELG9CQUFnQixDQUFDNEQsVUFBakIsQ0FBNEIsVUFBNUI7QUFDRCxHQUhELE1BR08sSUFBR3pCLEtBQUssR0FBRyxDQUFYLEVBQWM7QUFDbkJsQyxnQkFBWSxDQUFDNEQsSUFBYixDQUFrQixVQUFsQixFQUE4QixVQUE5QjtBQUNBN0Qsb0JBQWdCLENBQUM0RCxVQUFqQixDQUE0QixVQUE1QjtBQUNELEdBSE0sTUFHQTtBQUNMM0QsZ0JBQVksQ0FBQzJELFVBQWIsQ0FBd0IsVUFBeEI7QUFDQTVELG9CQUFnQixDQUFDNkQsSUFBakIsQ0FBc0IsVUFBdEIsRUFBa0MsVUFBbEM7QUFDRDtBQUNGOztBQUVEWixTQUFTLEdBQUduQyxJQUFaLENBQWlCLFVBQUNnRCxHQUFELEVBQVM7QUFDeEJBLEtBQUcsQ0FBQ0MsTUFBSixDQUFXMUMsT0FBWCxDQUFtQixVQUFDQyxJQUFELEVBQU9DLENBQVAsRUFBYTtBQUM5QjFCLFdBQU8sQ0FBQzRCLE1BQVIsMkJBQWlDcUMsR0FBRyxDQUFDRSxLQUFKLENBQVV6QyxDQUFWLENBQWpDLGdCQUFrREQsSUFBbEQ7QUFDRCxHQUZEO0FBR0QsQ0FKRCxXQUlTSSxPQUFPLENBQUNDLEtBSmpCLEU7Ozs7Ozs7Ozs7O0FDbkxBLHVDOzs7Ozs7Ozs7OztBQ0FBLHNCQUFzQixtQkFBTyxDQUFDLDZGQUFnQzs7QUFFOUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsS0FBSyxpQkFBaUI7QUFDdEIsR0FBRztBQUNIOzs7Ozs7Ozs7Ozs7QUNkQSxlQUFlLG1CQUFPLENBQUMsNkVBQXdCOztBQUUvQztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7Ozs7Ozs7Ozs7Ozs7QUNOYTtBQUNiLFFBQVEsbUJBQU8sQ0FBQyx1RUFBcUI7QUFDckMsY0FBYyxtQkFBTyxDQUFDLHlGQUE4QjtBQUNwRCxtQ0FBbUMsbUJBQU8sQ0FBQywySEFBK0M7QUFDMUYsOEJBQThCLG1CQUFPLENBQUMsaUhBQTBDOztBQUVoRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRyxnRkFBZ0Y7QUFDbkY7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7OztBQ2pCWTtBQUNiLFFBQVEsbUJBQU8sQ0FBQyx1RUFBcUI7QUFDckMsZ0JBQWdCLG1CQUFPLENBQUMsdUZBQTZCO0FBQ3JELHVCQUF1QixtQkFBTyxDQUFDLCtGQUFpQztBQUNoRSw4QkFBOEIsbUJBQU8sQ0FBQyxpSEFBMEM7O0FBRWhGLHlEQUF5RCx3QkFBd0I7O0FBRWpGO0FBQ0E7QUFDQSxHQUFHLHdEQUF3RDtBQUMzRDtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNqQmE7QUFDYixRQUFRLG1CQUFPLENBQUMsdUVBQXFCO0FBQ3JDLGlCQUFpQixtQkFBTyxDQUFDLG1GQUEyQjtBQUNwRCw2QkFBNkIsbUJBQU8sQ0FBQywyR0FBdUM7QUFDNUUsMkJBQTJCLG1CQUFPLENBQUMseUdBQXNDOztBQUV6RTtBQUNBO0FBQ0EsR0FBRywyRUFBMkU7QUFDOUU7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOzs7Ozs7Ozs7Ozs7O0FDYlk7QUFDYixvQ0FBb0MsbUJBQU8sQ0FBQywrSEFBaUQ7QUFDN0YsZUFBZSxtQkFBTyxDQUFDLDZFQUF3QjtBQUMvQyxlQUFlLG1CQUFPLENBQUMsNkVBQXdCO0FBQy9DLDZCQUE2QixtQkFBTyxDQUFDLDJHQUF1QztBQUM1RSx5QkFBeUIsbUJBQU8sQ0FBQyxtR0FBbUM7QUFDcEUsaUJBQWlCLG1CQUFPLENBQUMsbUdBQW1DOztBQUU1RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOzs7Ozs7Ozs7Ozs7QUMzQ0QsUUFBUSxtQkFBTyxDQUFDLHVFQUFxQjtBQUNyQyxhQUFhLG1CQUFPLENBQUMsdUVBQXFCO0FBQzFDLGdCQUFnQixtQkFBTyxDQUFDLDZGQUFnQzs7QUFFeEQ7QUFDQSxzQ0FBc0M7O0FBRXRDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUcseUNBQXlDO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMiLCJmaWxlIjoiaG9tZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAkIGZyb20gJ2pxdWVyeSc7XHJcblxyXG5pbXBvcnQgJy4uL3N0eWxlcy9ob21lLmNzcyc7XHJcblxyXG5sZXQgZGF0YV9jYWNoZSA9IHsgZG9jdW1lbnRzIDogW119XHJcbmxldCAkc3BlYWtlcnNfc2VsZWN0ID0gJChcIiNzcGVha2VyLXNlbGVjdFwiKVxyXG5sZXQgJHRleHRDb250ZW50ICAgICA9ICQoXCIjdGV4dC1jb250ZW50XCIpXHJcbmxldCAkc2VhcmNoICAgICAgICAgID0gJChcIiNzZWFyY2gtYnRuXCIpXHJcbmxldCAkc2VsZWN0ICAgICAgICAgID0gJChcIiNkYXktc2VsZWN0XCIpXHJcbmxldCAkd29yZF9zZWFyY2ggICAgID0gJChcIiN3b3JkLXNlYXJjaFwiKVxyXG5sZXQgJHJlc3VsdF9udW1iZXIgICA9ICQoXCIjcmVzdWx0LW51bWJlclwiKVxyXG5sZXQgJHByZXZpb3VzX2J1dHRvbiA9ICQoXCIjcHJldmlvdXMtYnV0dG9uXCIpXHJcbmxldCAkbmV4dF9idXR0b24gICAgID0gJChcIiNuZXh0LWJ1dHRvblwiKVxyXG5sZXQgJGRheV90aXRsZSAgICAgICA9ICQoXCIjZGF5LXRpdGxlXCIpXHJcbmxldCAkbG9hZGVyICAgICAgICAgID0gJChcIiNsb2FkZXJcIilcclxubGV0IGRheV9pbmRleCAgICAgICAgPSAwXHJcbmxldCB0aW1lb3V0ICAgICAgICAgID0gbnVsbFxyXG5cclxuJHNlbGVjdC5vbihcImNoYW5nZVwiLCBmdW5jdGlvbigpIHtcclxuICBsZXQgc3BlYWtlcnMgPSBbXVxyXG4gIGRheV9pbmRleCA9ICQodGhpcykucHJvcCgnc2VsZWN0ZWRJbmRleCcpO1xyXG4gICR0ZXh0Q29udGVudC5odG1sKFwiXCIpXHJcbiAgJGRheV90aXRsZS5odG1sKFwiXCIpXHJcbiAgJGxvYWRlci5jc3MoXCJkaXNwbGF5XCIsIFwiYmxvY2tcIilcclxuICAkc3BlYWtlcnNfc2VsZWN0Lmh0bWwoXCJcIilcclxuICBkaXNwbGF5TmF2aWdhdGlvbkJ1dHRvbihkYXlfaW5kZXgpXHJcblxyXG4gIGdldEFydGljbGUoJHNlbGVjdC52YWwoKSkudGhlbihmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAkZGF5X3RpdGxlLnRleHQoJChcIiNkYXktc2VsZWN0IG9wdGlvbjpzZWxlY3RlZFwiKS50ZXh0KCkpXHJcbiAgICAvKiBjYWNoZSB0aGUgcmVzdWx0ICovXHJcbiAgICBkYXRhX2NhY2hlLmRvY3VtZW50cy5wdXNoKHt0aXRsZTogJHNlbGVjdC52YWwoKSwgZDogZGF0YX0pXHJcbiAgICAkbG9hZGVyLmNzcyhcImRpc3BsYXlcIiwgXCJub25lXCIpXHJcbiAgICBkYXRhLnNwLmZvckVhY2goKGl0ZW0sIGkpID0+IHtcclxuICAgICAgaWYoIXNwZWFrZXJzLmluY2x1ZGVzKGl0ZW0pKSB7XHJcbiAgICAgICAgc3BlYWtlcnMucHVzaChpdGVtKVxyXG4gICAgICAgICRzcGVha2Vyc19zZWxlY3QuYXBwZW5kKGA8b3B0aW9uPiR7aXRlbX08L29wdGlvbj5gKVxyXG4gICAgICB9XHJcbiAgICAgICR0ZXh0Q29udGVudC5hcHBlbmQoYDxoNT4ke2l0ZW19PC9oNT48cD4ke2RhdGEudGV4dFtpXX08L3A+YClcclxuICAgIH0pO1xyXG4gIH0pLmNhdGNoKGNvbnNvbGUuZXJyb3IpXHJcbn0pXHJcblxyXG4kd29yZF9zZWFyY2gub24oXCJpbnB1dFwiLCBmdW5jdGlvbigpIHtcclxuICBpZih0aW1lb3V0ICE9PSBudWxsKSB7XHJcbiAgICBjbGVhclRpbWVvdXQodGltZW91dClcclxuICB9XHJcbiAgdGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgbGV0IG9jY3VyZW5jZXMgPSAwXHJcbiAgICBsZXQgdmFsdWUgICAgICA9ICQodGhpcykudmFsKClcclxuICAgIGxldCBwYXJhZ3JhcGhzID0gJHRleHRDb250ZW50LmZpbmQoXCJwXCIpXHJcbiAgICBsZXQgc3BlYWtlcnMgICA9ICR0ZXh0Q29udGVudC5maW5kKFwiaDVcIilcclxuXHJcbiAgICBwYXJhZ3JhcGhzLmVhY2goZnVuY3Rpb24oaW5kZXgpIHtcclxuICAgICAgbGV0IHRleHQgPSAkKHRoaXMpLnRleHQoKS50b0xvd2VyQ2FzZSgpXHJcbiAgICAgIGlmKCF0ZXh0LmluY2x1ZGVzKHZhbHVlLnRvTG93ZXJDYXNlKCkpKSB7XHJcbiAgICAgICAgJCh0aGlzKS5jc3MoXCJkaXNwbGF5XCIsIFwibm9uZVwiKVxyXG4gICAgICAgICQoc3BlYWtlcnNbaW5kZXhdKS5jc3MoXCJkaXNwbGF5XCIsIFwibm9uZVwiKVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICQodGhpcykuY3NzKFwiZGlzcGxheVwiLCBcImJsb2NrXCIpXHJcbiAgICAgICAgJChzcGVha2Vyc1tpbmRleF0pLmNzcyhcImRpc3BsYXlcIiwgXCJibG9ja1wiKVxyXG4gICAgICAgIC8qIGhpZ2hsaWdodCBpbiB0ZXh0ICovXHJcbiAgICAgICAgbGV0IHJlZyA9IG5ldyBSZWdFeHAoJygnK3ZhbHVlKycpJywgJ2dpJyk7XHJcbiAgICAgICAgb2NjdXJlbmNlcyArPSAoJCh0aGlzKS50ZXh0KCkubWF0Y2gocmVnKSB8fCBbXSkubGVuZ3RoXHJcbiAgICAgICAgbGV0IG5ld190ZXh0ID0gJCh0aGlzKS50ZXh0KCkucmVwbGFjZShyZWcsIGA8c3BhbiBjbGFzcz1cInNlYXJjaC1pdGVtXCI+JDE8L3NwYW4+YClcclxuICAgICAgICAkKHRoaXMpLmh0bWwobmV3X3RleHQpXHJcbiAgICAgIH1cclxuICAgIH0pXHJcblxyXG4gICAgaWYodmFsdWUgIT09IFwiXCIgJiYgb2NjdXJlbmNlcyAhPSAwKSB7XHJcbiAgICAgICRyZXN1bHRfbnVtYmVyLnRleHQoYExlIHRlcm1lIGFwcGFyYcOudCAke29jY3VyZW5jZXN9IGZvaXNgKVxyXG4gICAgfSBlbHNlIGlmKG9jY3VyZW5jZXMgPT0gMCl7XHJcbiAgICAgICRyZXN1bHRfbnVtYmVyLnRleHQoYEF1Y3VuIHLDqXN1bHRhdCBwb3VyIGNlIHRlcm1lYClcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICRyZXN1bHRfbnVtYmVyLnRleHQoJycpXHJcbiAgICB9XHJcbiAgfSwgMjAwKTtcclxufSlcclxuXHJcbiRzcGVha2Vyc19zZWxlY3Qub24oXCJjaGFuZ2VcIiwgZnVuY3Rpb24oKSB7XHJcbiAgbGV0IHZhbHVlID0gJHNlbGVjdC52YWwoKTtcclxuICBsZXQgJGN1cnJlbnRfc3BlYWtlciA9ICQodGhpcykudmFsKClcclxuICAkdGV4dENvbnRlbnQuaHRtbChcIlwiKVxyXG5cclxuICAvKiBsb2FkIHRleHQgaW4gY2FjaGUgKi9cclxuICBsZXQgZG9jX2ZpbHRlcmVkID0gZGF0YV9jYWNoZS5kb2N1bWVudHMuZmlsdGVyKGZ1bmN0aW9uKGRvYykge1xyXG4gICAgcmV0dXJuIGRvYy50aXRsZSA9PT0gdmFsdWVcclxuICB9KVxyXG5cclxuICBkb2NfZmlsdGVyZWRbMF0uZC5zcC5mb3JFYWNoKChpdGVtLCBpKSA9PiB7XHJcbiAgICBpZihpdGVtID09ICRjdXJyZW50X3NwZWFrZXIpIHtcclxuICAgICAgJHRleHRDb250ZW50LmFwcGVuZChgPGg1PiR7aXRlbX08L2g1PjxwPiR7ZG9jX2ZpbHRlcmVkWzBdLmQudGV4dFtpXX08L3A+YClcclxuICAgIH1cclxuICB9KTtcclxufSlcclxuXHJcbiRwcmV2aW91c19idXR0b24ub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcclxuICBkYXlfaW5kZXggLT0gMVxyXG4gIGRpc3BsYXlOYXZpZ2F0aW9uQnV0dG9uKGRheV9pbmRleClcclxuICBsb2FkVGV4dEZyb21JbmRleChkYXlfaW5kZXgpXHJcbiAgbGV0ICRvcHRpb24gPSAkc2VsZWN0LmZpbmQoYG9wdGlvbjpudGgtY2hpbGQoJHtkYXlfaW5kZXgrMX0pYClcclxuICAkb3B0aW9uLnByb3AoJ3NlbGVjdGVkJywgdHJ1ZSlcclxufSlcclxuXHJcbiRuZXh0X2J1dHRvbi5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKCkge1xyXG4gIGRheV9pbmRleCArPSAxXHJcbiAgZGlzcGxheU5hdmlnYXRpb25CdXR0b24oZGF5X2luZGV4KVxyXG4gIGxvYWRUZXh0RnJvbUluZGV4KGRheV9pbmRleClcclxuICBsZXQgJG9wdGlvbiA9ICRzZWxlY3QuZmluZChgb3B0aW9uOm50aC1jaGlsZCgke2RheV9pbmRleCsxfSlgKVxyXG4gICRvcHRpb24ucHJvcCgnc2VsZWN0ZWQnLCB0cnVlKVxyXG59KVxyXG5cclxuZnVuY3Rpb24gbG9hZFRleHRGcm9tSW5kZXgoaW5kZXgpIHtcclxuICBsZXQgc3BlYWtlcnMgPSBbXVxyXG4gICR0ZXh0Q29udGVudC5odG1sKFwiXCIpXHJcbiAgJHNwZWFrZXJzX3NlbGVjdC5odG1sKFwiXCIpXHJcbiAgJGRheV90aXRsZS5odG1sKFwiXCIpXHJcbiAgJGxvYWRlci5jc3MoXCJkaXNwbGF5XCIsIFwiYmxvY2tcIilcclxuICBsZXQgJG9wdGlvbiA9ICRzZWxlY3QuZmluZChgb3B0aW9uOm50aC1jaGlsZCgke2RheV9pbmRleCsxfSlgKVxyXG4gIGxldCB2YWx1ZSAgPSAkb3B0aW9uLnZhbCgpXHJcbiAgbGV0IHRleHQgPSAkb3B0aW9uLnRleHQoKVxyXG4gIGdldEFydGljbGUodmFsdWUpLnRoZW4oZnVuY3Rpb24oZGF0YSkge1xyXG4gICAgJGxvYWRlci5jc3MoXCJkaXNwbGF5XCIsIFwibm9uZVwiKVxyXG4gICAgJHRleHRDb250ZW50LnRleHQoXCJcIilcclxuICAgICRkYXlfdGl0bGUudGV4dCh0ZXh0KVxyXG4gICAgZGF0YS5zcC5mb3JFYWNoKChpdGVtLCBpKSA9PiB7XHJcbiAgICAgIGlmKCFzcGVha2Vycy5pbmNsdWRlcyhpdGVtKSkge1xyXG4gICAgICAgIHNwZWFrZXJzLnB1c2goaXRlbSlcclxuICAgICAgICAkc3BlYWtlcnNfc2VsZWN0LmFwcGVuZChgPG9wdGlvbj4ke2l0ZW19PC9vcHRpb24+YClcclxuICAgICAgfVxyXG4gICAgICAkdGV4dENvbnRlbnQuYXBwZW5kKGA8aDU+JHtpdGVtfTwvaDU+PHA+JHtkYXRhLnRleHRbaV19PC9wPmApXHJcbiAgICB9KTtcclxuICB9KS5jYXRjaChjb25zb2xlLmVycm9yKVxyXG59XHJcblxyXG4vKiBGdW5jdGlvbnMgKi9cclxuZnVuY3Rpb24gZ2V0VGl0bGVzKCkge1xyXG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAkLmFqYXgoe1xyXG4gICAgICAgdXJsIDogJy9hcGkvdGl0bGVzJyxcclxuICAgICAgIHR5cGUgOiAnR0VUJyxcclxuICAgICAgIHN1Y2Nlc3MgOiBmdW5jdGlvbihyZXMpe1xyXG4gICAgICAgICByZXNvbHZlKHJlcylcclxuICAgICAgIH0sXHJcbiAgICAgICBlcnJvciA6IGZ1bmN0aW9uKHJlcywgc3RhdHVzLCBlcnIpe1xyXG4gICAgICAgICByZWplY3QoZXJyKVxyXG4gICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfSlcclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0QXJ0aWNsZSh0aXRsZSkge1xyXG4gIHRpdGxlID0gdGl0bGUucmVwbGFjZShcIi54bWxcIiwgXCJcIilcclxuICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgJC5hamF4KHtcclxuICAgICAgIHVybCA6IGAvYXBpL2RvY3VtZW50LyR7dGl0bGV9YCxcclxuICAgICAgIHR5cGUgOiAnR0VUJyxcclxuICAgICAgIHN1Y2Nlc3MgOiBmdW5jdGlvbihyZXMpe1xyXG4gICAgICAgICByZXNvbHZlKHJlcylcclxuICAgICAgIH0sXHJcbiAgICAgICBlcnJvciA6IGZ1bmN0aW9uKHJlcywgc3RhdHVzLCBlcnIpe1xyXG4gICAgICAgICByZWplY3QoZXJyKVxyXG4gICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfSlcclxufVxyXG5cclxuZnVuY3Rpb24gZGlzcGxheU5hdmlnYXRpb25CdXR0b24oaW5kZXgpIHtcclxuICBpZihpbmRleCA+IDEgJiYgaW5kZXggPCAyMjEpIHtcclxuICAgICRuZXh0X2J1dHRvbi5yZW1vdmVBdHRyKFwiZGlzYWJsZWRcIilcclxuICAgICRwcmV2aW91c19idXR0b24ucmVtb3ZlQXR0cihcImRpc2FibGVkXCIpXHJcbiAgfSBlbHNlIGlmKGluZGV4ID4gMSkge1xyXG4gICAgJG5leHRfYnV0dG9uLmF0dHIoXCJkaXNhYmxlZFwiLCBcImRpc2FibGVkXCIpXHJcbiAgICAkcHJldmlvdXNfYnV0dG9uLnJlbW92ZUF0dHIoXCJkaXNhYmxlZFwiKVxyXG4gIH0gZWxzZSB7XHJcbiAgICAkbmV4dF9idXR0b24ucmVtb3ZlQXR0cihcImRpc2FibGVkXCIpXHJcbiAgICAkcHJldmlvdXNfYnV0dG9uLmF0dHIoXCJkaXNhYmxlZFwiLCBcImRpc2FibGVkXCIpXHJcbiAgfVxyXG59XHJcblxyXG5nZXRUaXRsZXMoKS50aGVuKChvcHQpID0+IHtcclxuICBvcHQudGl0bGVzLmZvckVhY2goKGl0ZW0sIGkpID0+IHtcclxuICAgICRzZWxlY3QuYXBwZW5kKGA8b3B0aW9uIHZhbHVlPVwiJHtvcHQucGF0aHNbaV19XCI+JHtpdGVtfTwvb3B0aW9uPmApXHJcbiAgfSk7XHJcbn0pLmNhdGNoKGNvbnNvbGUuZXJyb3IpXHJcbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiIsInZhciB3ZWxsS25vd25TeW1ib2wgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvd2VsbC1rbm93bi1zeW1ib2wnKTtcblxudmFyIE1BVENIID0gd2VsbEtub3duU3ltYm9sKCdtYXRjaCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChNRVRIT0RfTkFNRSkge1xuICB2YXIgcmVnZXhwID0gLy4vO1xuICB0cnkge1xuICAgICcvLi8nW01FVEhPRF9OQU1FXShyZWdleHApO1xuICB9IGNhdGNoIChlcnJvcjEpIHtcbiAgICB0cnkge1xuICAgICAgcmVnZXhwW01BVENIXSA9IGZhbHNlO1xuICAgICAgcmV0dXJuICcvLi8nW01FVEhPRF9OQU1FXShyZWdleHApO1xuICAgIH0gY2F0Y2ggKGVycm9yMikgeyAvKiBlbXB0eSAqLyB9XG4gIH0gcmV0dXJuIGZhbHNlO1xufTtcbiIsInZhciBpc1JlZ0V4cCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9pcy1yZWdleHAnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgaWYgKGlzUmVnRXhwKGl0KSkge1xuICAgIHRocm93IFR5cGVFcnJvcihcIlRoZSBtZXRob2QgZG9lc24ndCBhY2NlcHQgcmVndWxhciBleHByZXNzaW9uc1wiKTtcbiAgfSByZXR1cm4gaXQ7XG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xudmFyICQgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvZXhwb3J0Jyk7XG52YXIgJGZpbHRlciA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9hcnJheS1pdGVyYXRpb24nKS5maWx0ZXI7XG52YXIgYXJyYXlNZXRob2RIYXNTcGVjaWVzU3VwcG9ydCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9hcnJheS1tZXRob2QtaGFzLXNwZWNpZXMtc3VwcG9ydCcpO1xudmFyIGFycmF5TWV0aG9kVXNlc1RvTGVuZ3RoID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2FycmF5LW1ldGhvZC11c2VzLXRvLWxlbmd0aCcpO1xuXG52YXIgSEFTX1NQRUNJRVNfU1VQUE9SVCA9IGFycmF5TWV0aG9kSGFzU3BlY2llc1N1cHBvcnQoJ2ZpbHRlcicpO1xuLy8gRWRnZSAxNC0gaXNzdWVcbnZhciBVU0VTX1RPX0xFTkdUSCA9IGFycmF5TWV0aG9kVXNlc1RvTGVuZ3RoKCdmaWx0ZXInKTtcblxuLy8gYEFycmF5LnByb3RvdHlwZS5maWx0ZXJgIG1ldGhvZFxuLy8gaHR0cHM6Ly90YzM5LmdpdGh1Yi5pby9lY21hMjYyLyNzZWMtYXJyYXkucHJvdG90eXBlLmZpbHRlclxuLy8gd2l0aCBhZGRpbmcgc3VwcG9ydCBvZiBAQHNwZWNpZXNcbiQoeyB0YXJnZXQ6ICdBcnJheScsIHByb3RvOiB0cnVlLCBmb3JjZWQ6ICFIQVNfU1BFQ0lFU19TVVBQT1JUIHx8ICFVU0VTX1RPX0xFTkdUSCB9LCB7XG4gIGZpbHRlcjogZnVuY3Rpb24gZmlsdGVyKGNhbGxiYWNrZm4gLyogLCB0aGlzQXJnICovKSB7XG4gICAgcmV0dXJuICRmaWx0ZXIodGhpcywgY2FsbGJhY2tmbiwgYXJndW1lbnRzLmxlbmd0aCA+IDEgPyBhcmd1bWVudHNbMV0gOiB1bmRlZmluZWQpO1xuICB9XG59KTtcbiIsIid1c2Ugc3RyaWN0JztcbnZhciAkID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2V4cG9ydCcpO1xudmFyICRpbmNsdWRlcyA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9hcnJheS1pbmNsdWRlcycpLmluY2x1ZGVzO1xudmFyIGFkZFRvVW5zY29wYWJsZXMgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvYWRkLXRvLXVuc2NvcGFibGVzJyk7XG52YXIgYXJyYXlNZXRob2RVc2VzVG9MZW5ndGggPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvYXJyYXktbWV0aG9kLXVzZXMtdG8tbGVuZ3RoJyk7XG5cbnZhciBVU0VTX1RPX0xFTkdUSCA9IGFycmF5TWV0aG9kVXNlc1RvTGVuZ3RoKCdpbmRleE9mJywgeyBBQ0NFU1NPUlM6IHRydWUsIDE6IDAgfSk7XG5cbi8vIGBBcnJheS5wcm90b3R5cGUuaW5jbHVkZXNgIG1ldGhvZFxuLy8gaHR0cHM6Ly90YzM5LmdpdGh1Yi5pby9lY21hMjYyLyNzZWMtYXJyYXkucHJvdG90eXBlLmluY2x1ZGVzXG4kKHsgdGFyZ2V0OiAnQXJyYXknLCBwcm90bzogdHJ1ZSwgZm9yY2VkOiAhVVNFU19UT19MRU5HVEggfSwge1xuICBpbmNsdWRlczogZnVuY3Rpb24gaW5jbHVkZXMoZWwgLyogLCBmcm9tSW5kZXggPSAwICovKSB7XG4gICAgcmV0dXJuICRpbmNsdWRlcyh0aGlzLCBlbCwgYXJndW1lbnRzLmxlbmd0aCA+IDEgPyBhcmd1bWVudHNbMV0gOiB1bmRlZmluZWQpO1xuICB9XG59KTtcblxuLy8gaHR0cHM6Ly90YzM5LmdpdGh1Yi5pby9lY21hMjYyLyNzZWMtYXJyYXkucHJvdG90eXBlLUBAdW5zY29wYWJsZXNcbmFkZFRvVW5zY29wYWJsZXMoJ2luY2x1ZGVzJyk7XG4iLCIndXNlIHN0cmljdCc7XG52YXIgJCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9leHBvcnQnKTtcbnZhciBub3RBUmVnRXhwID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL25vdC1hLXJlZ2V4cCcpO1xudmFyIHJlcXVpcmVPYmplY3RDb2VyY2libGUgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvcmVxdWlyZS1vYmplY3QtY29lcmNpYmxlJyk7XG52YXIgY29ycmVjdElzUmVnRXhwTG9naWMgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvY29ycmVjdC1pcy1yZWdleHAtbG9naWMnKTtcblxuLy8gYFN0cmluZy5wcm90b3R5cGUuaW5jbHVkZXNgIG1ldGhvZFxuLy8gaHR0cHM6Ly90YzM5LmdpdGh1Yi5pby9lY21hMjYyLyNzZWMtc3RyaW5nLnByb3RvdHlwZS5pbmNsdWRlc1xuJCh7IHRhcmdldDogJ1N0cmluZycsIHByb3RvOiB0cnVlLCBmb3JjZWQ6ICFjb3JyZWN0SXNSZWdFeHBMb2dpYygnaW5jbHVkZXMnKSB9LCB7XG4gIGluY2x1ZGVzOiBmdW5jdGlvbiBpbmNsdWRlcyhzZWFyY2hTdHJpbmcgLyogLCBwb3NpdGlvbiA9IDAgKi8pIHtcbiAgICByZXR1cm4gISF+U3RyaW5nKHJlcXVpcmVPYmplY3RDb2VyY2libGUodGhpcykpXG4gICAgICAuaW5kZXhPZihub3RBUmVnRXhwKHNlYXJjaFN0cmluZyksIGFyZ3VtZW50cy5sZW5ndGggPiAxID8gYXJndW1lbnRzWzFdIDogdW5kZWZpbmVkKTtcbiAgfVxufSk7XG4iLCIndXNlIHN0cmljdCc7XG52YXIgZml4UmVnRXhwV2VsbEtub3duU3ltYm9sTG9naWMgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvZml4LXJlZ2V4cC13ZWxsLWtub3duLXN5bWJvbC1sb2dpYycpO1xudmFyIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2FuLW9iamVjdCcpO1xudmFyIHRvTGVuZ3RoID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL3RvLWxlbmd0aCcpO1xudmFyIHJlcXVpcmVPYmplY3RDb2VyY2libGUgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvcmVxdWlyZS1vYmplY3QtY29lcmNpYmxlJyk7XG52YXIgYWR2YW5jZVN0cmluZ0luZGV4ID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2FkdmFuY2Utc3RyaW5nLWluZGV4Jyk7XG52YXIgcmVnRXhwRXhlYyA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9yZWdleHAtZXhlYy1hYnN0cmFjdCcpO1xuXG4vLyBAQG1hdGNoIGxvZ2ljXG5maXhSZWdFeHBXZWxsS25vd25TeW1ib2xMb2dpYygnbWF0Y2gnLCAxLCBmdW5jdGlvbiAoTUFUQ0gsIG5hdGl2ZU1hdGNoLCBtYXliZUNhbGxOYXRpdmUpIHtcbiAgcmV0dXJuIFtcbiAgICAvLyBgU3RyaW5nLnByb3RvdHlwZS5tYXRjaGAgbWV0aG9kXG4gICAgLy8gaHR0cHM6Ly90YzM5LmdpdGh1Yi5pby9lY21hMjYyLyNzZWMtc3RyaW5nLnByb3RvdHlwZS5tYXRjaFxuICAgIGZ1bmN0aW9uIG1hdGNoKHJlZ2V4cCkge1xuICAgICAgdmFyIE8gPSByZXF1aXJlT2JqZWN0Q29lcmNpYmxlKHRoaXMpO1xuICAgICAgdmFyIG1hdGNoZXIgPSByZWdleHAgPT0gdW5kZWZpbmVkID8gdW5kZWZpbmVkIDogcmVnZXhwW01BVENIXTtcbiAgICAgIHJldHVybiBtYXRjaGVyICE9PSB1bmRlZmluZWQgPyBtYXRjaGVyLmNhbGwocmVnZXhwLCBPKSA6IG5ldyBSZWdFeHAocmVnZXhwKVtNQVRDSF0oU3RyaW5nKE8pKTtcbiAgICB9LFxuICAgIC8vIGBSZWdFeHAucHJvdG90eXBlW0BAbWF0Y2hdYCBtZXRob2RcbiAgICAvLyBodHRwczovL3RjMzkuZ2l0aHViLmlvL2VjbWEyNjIvI3NlYy1yZWdleHAucHJvdG90eXBlLUBAbWF0Y2hcbiAgICBmdW5jdGlvbiAocmVnZXhwKSB7XG4gICAgICB2YXIgcmVzID0gbWF5YmVDYWxsTmF0aXZlKG5hdGl2ZU1hdGNoLCByZWdleHAsIHRoaXMpO1xuICAgICAgaWYgKHJlcy5kb25lKSByZXR1cm4gcmVzLnZhbHVlO1xuXG4gICAgICB2YXIgcnggPSBhbk9iamVjdChyZWdleHApO1xuICAgICAgdmFyIFMgPSBTdHJpbmcodGhpcyk7XG5cbiAgICAgIGlmICghcnguZ2xvYmFsKSByZXR1cm4gcmVnRXhwRXhlYyhyeCwgUyk7XG5cbiAgICAgIHZhciBmdWxsVW5pY29kZSA9IHJ4LnVuaWNvZGU7XG4gICAgICByeC5sYXN0SW5kZXggPSAwO1xuICAgICAgdmFyIEEgPSBbXTtcbiAgICAgIHZhciBuID0gMDtcbiAgICAgIHZhciByZXN1bHQ7XG4gICAgICB3aGlsZSAoKHJlc3VsdCA9IHJlZ0V4cEV4ZWMocngsIFMpKSAhPT0gbnVsbCkge1xuICAgICAgICB2YXIgbWF0Y2hTdHIgPSBTdHJpbmcocmVzdWx0WzBdKTtcbiAgICAgICAgQVtuXSA9IG1hdGNoU3RyO1xuICAgICAgICBpZiAobWF0Y2hTdHIgPT09ICcnKSByeC5sYXN0SW5kZXggPSBhZHZhbmNlU3RyaW5nSW5kZXgoUywgdG9MZW5ndGgocngubGFzdEluZGV4KSwgZnVsbFVuaWNvZGUpO1xuICAgICAgICBuKys7XG4gICAgICB9XG4gICAgICByZXR1cm4gbiA9PT0gMCA/IG51bGwgOiBBO1xuICAgIH1cbiAgXTtcbn0pO1xuIiwidmFyICQgPSByZXF1aXJlKCcuLi9pbnRlcm5hbHMvZXhwb3J0Jyk7XG52YXIgZ2xvYmFsID0gcmVxdWlyZSgnLi4vaW50ZXJuYWxzL2dsb2JhbCcpO1xudmFyIHVzZXJBZ2VudCA9IHJlcXVpcmUoJy4uL2ludGVybmFscy9lbmdpbmUtdXNlci1hZ2VudCcpO1xuXG52YXIgc2xpY2UgPSBbXS5zbGljZTtcbnZhciBNU0lFID0gL01TSUUgLlxcLi8udGVzdCh1c2VyQWdlbnQpOyAvLyA8LSBkaXJ0eSBpZTktIGNoZWNrXG5cbnZhciB3cmFwID0gZnVuY3Rpb24gKHNjaGVkdWxlcikge1xuICByZXR1cm4gZnVuY3Rpb24gKGhhbmRsZXIsIHRpbWVvdXQgLyogLCAuLi5hcmd1bWVudHMgKi8pIHtcbiAgICB2YXIgYm91bmRBcmdzID0gYXJndW1lbnRzLmxlbmd0aCA+IDI7XG4gICAgdmFyIGFyZ3MgPSBib3VuZEFyZ3MgPyBzbGljZS5jYWxsKGFyZ3VtZW50cywgMikgOiB1bmRlZmluZWQ7XG4gICAgcmV0dXJuIHNjaGVkdWxlcihib3VuZEFyZ3MgPyBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tbmV3LWZ1bmNcbiAgICAgICh0eXBlb2YgaGFuZGxlciA9PSAnZnVuY3Rpb24nID8gaGFuZGxlciA6IEZ1bmN0aW9uKGhhbmRsZXIpKS5hcHBseSh0aGlzLCBhcmdzKTtcbiAgICB9IDogaGFuZGxlciwgdGltZW91dCk7XG4gIH07XG59O1xuXG4vLyBpZTktIHNldFRpbWVvdXQgJiBzZXRJbnRlcnZhbCBhZGRpdGlvbmFsIHBhcmFtZXRlcnMgZml4XG4vLyBodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnL211bHRpcGFnZS90aW1lcnMtYW5kLXVzZXItcHJvbXB0cy5odG1sI3RpbWVyc1xuJCh7IGdsb2JhbDogdHJ1ZSwgYmluZDogdHJ1ZSwgZm9yY2VkOiBNU0lFIH0sIHtcbiAgLy8gYHNldFRpbWVvdXRgIG1ldGhvZFxuICAvLyBodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnL211bHRpcGFnZS90aW1lcnMtYW5kLXVzZXItcHJvbXB0cy5odG1sI2RvbS1zZXR0aW1lb3V0XG4gIHNldFRpbWVvdXQ6IHdyYXAoZ2xvYmFsLnNldFRpbWVvdXQpLFxuICAvLyBgc2V0SW50ZXJ2YWxgIG1ldGhvZFxuICAvLyBodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnL211bHRpcGFnZS90aW1lcnMtYW5kLXVzZXItcHJvbXB0cy5odG1sI2RvbS1zZXRpbnRlcnZhbFxuICBzZXRJbnRlcnZhbDogd3JhcChnbG9iYWwuc2V0SW50ZXJ2YWwpXG59KTtcbiJdLCJzb3VyY2VSb290IjoiIn0=