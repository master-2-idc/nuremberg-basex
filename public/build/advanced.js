(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["advanced"],{

/***/ "./assets/js/advanced.js":
/*!*******************************!*\
  !*** ./assets/js/advanced.js ***!
  \*******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _styles_advanced_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../styles/advanced.css */ "./assets/styles/advanced.css");
/* harmony import */ var _styles_advanced_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_advanced_css__WEBPACK_IMPORTED_MODULE_6__);







var $search_input = jquery__WEBPACK_IMPORTED_MODULE_5___default()("#search-input");
var $search = jquery__WEBPACK_IMPORTED_MODULE_5___default()("#search-btn");
var $result_ctn = jquery__WEBPACK_IMPORTED_MODULE_5___default()("#results-container");
var $loader = jquery__WEBPACK_IMPORTED_MODULE_5___default()("#loader");
var $speaker_select = jquery__WEBPACK_IMPORTED_MODULE_5___default()("#speaker-select");
var $result_number_ctn = jquery__WEBPACK_IMPORTED_MODULE_5___default()(".result-number-container");
var $mode_select = jquery__WEBPACK_IMPORTED_MODULE_5___default()("#mode-select");
var $all_any_select = jquery__WEBPACK_IMPORTED_MODULE_5___default()("#all-any-select");
var mode = 'term';
/* loading all speakers for searching by speakers */

getAllSpeakers().then(function (response) {
  var speakers = response.speakers;
  speakers.forEach(function (speaker) {
    var $option = jquery__WEBPACK_IMPORTED_MODULE_5___default()("<option>").attr("value", speaker).text(speaker);
    $speaker_select.append($option);
  });
})["catch"](console.error);
$mode_select.on('change', function () {
  var selected = jquery__WEBPACK_IMPORTED_MODULE_5___default()(this).val();

  if (selected == 'term') {
    mode = 'term';
    $speaker_select.css('display', 'none');
    $search_input.css('display', 'block');
    $all_any_select.css('display', 'block');
  } else if (selected == 'speaker') {
    mode = 'speaker';
    $speaker_select.css('display', 'block');
    $search_input.css('display', 'none');
    $all_any_select.css('display', 'none');
  }
});
/* Event listeners */

$search.on("click", function (e) {
  e.preventDefault();
  var input_value = '';
  mode == 'term' ? input_value = $search_input.val() : input_value = $speaker_select.val();
  var all = $all_any_select.val();
  $result_ctn.html("");
  $loader.css("display", "block");
  getWithStemming(input_value, mode, all).then(function (res) {
    $loader.css("display", "none");
    res.data.forEach(function (item) {
      var html = "<div class=\"card\">\n          <div class=\"card-body\">\n            <h5 class=\"card-title\"><a href=\"detail/".concat(item.document, "?mode=").concat(mode, "&term=").concat(input_value, "&all=").concat(all, "\">").concat(item.title, "</a> (").concat(item.document, ")</h5>\n            <p class=\"card-text\">").concat(item.number, " paragraphes correspondant.</p>\n          </div>\n        </div>");
      $result_ctn.append(html);
    });
    $result_number_ctn.text("".concat(res.result_number, " r\xE9sultats trouv\xE9s dans ").concat(res.documents_len, " documents"));
  });
});

function getWithStemming(terms, mode, all) {
  return new Promise(function (resolve, reject) {
    jquery__WEBPACK_IMPORTED_MODULE_5___default.a.ajax({
      url: "/api/search/?mode=".concat(mode, "&term=").concat(terms, "&all=").concat(all),
      type: 'GET',
      success: function success(res) {
        resolve(res);
      },
      error: function error(res, status, err) {
        reject(err);
      }
    });
  });
}

function getAllSpeakers() {
  return new Promise(function (resolve, reject) {
    jquery__WEBPACK_IMPORTED_MODULE_5___default.a.ajax({
      url: "/api/speakers",
      type: 'GET',
      success: function success(res) {
        resolve(res);
      },
      error: function error(res, status, err) {
        reject(err);
      }
    });
  });
}

/***/ }),

/***/ "./assets/styles/advanced.css":
/*!************************************!*\
  !*** ./assets/styles/advanced.css ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./assets/js/advanced.js","runtime","vendors~advanced~app~detail~home","vendors~advanced~detail~home","vendors~advanced~home"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvYWR2YW5jZWQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3N0eWxlcy9hZHZhbmNlZC5jc3MiXSwibmFtZXMiOlsiJHNlYXJjaF9pbnB1dCIsIiQiLCIkc2VhcmNoIiwiJHJlc3VsdF9jdG4iLCIkbG9hZGVyIiwiJHNwZWFrZXJfc2VsZWN0IiwiJHJlc3VsdF9udW1iZXJfY3RuIiwiJG1vZGVfc2VsZWN0IiwiJGFsbF9hbnlfc2VsZWN0IiwibW9kZSIsImdldEFsbFNwZWFrZXJzIiwidGhlbiIsInJlc3BvbnNlIiwic3BlYWtlcnMiLCJmb3JFYWNoIiwic3BlYWtlciIsIiRvcHRpb24iLCJhdHRyIiwidGV4dCIsImFwcGVuZCIsImNvbnNvbGUiLCJlcnJvciIsIm9uIiwic2VsZWN0ZWQiLCJ2YWwiLCJjc3MiLCJlIiwicHJldmVudERlZmF1bHQiLCJpbnB1dF92YWx1ZSIsImFsbCIsImh0bWwiLCJnZXRXaXRoU3RlbW1pbmciLCJyZXMiLCJkYXRhIiwiaXRlbSIsImRvY3VtZW50IiwidGl0bGUiLCJudW1iZXIiLCJyZXN1bHRfbnVtYmVyIiwiZG9jdW1lbnRzX2xlbiIsInRlcm1zIiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJhamF4IiwidXJsIiwidHlwZSIsInN1Y2Nlc3MiLCJzdGF0dXMiLCJlcnIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBRUE7QUFFQSxJQUFNQSxhQUFhLEdBQUdDLDZDQUFDLENBQUMsZUFBRCxDQUF2QjtBQUNBLElBQU1DLE9BQU8sR0FBU0QsNkNBQUMsQ0FBQyxhQUFELENBQXZCO0FBQ0EsSUFBTUUsV0FBVyxHQUFLRiw2Q0FBQyxDQUFDLG9CQUFELENBQXZCO0FBQ0EsSUFBTUcsT0FBTyxHQUFTSCw2Q0FBQyxDQUFDLFNBQUQsQ0FBdkI7QUFDQSxJQUFNSSxlQUFlLEdBQUdKLDZDQUFDLENBQUMsaUJBQUQsQ0FBekI7QUFDQSxJQUFNSyxrQkFBa0IsR0FBR0wsNkNBQUMsQ0FBQywwQkFBRCxDQUE1QjtBQUNBLElBQU1NLFlBQVksR0FBR04sNkNBQUMsQ0FBQyxjQUFELENBQXRCO0FBQ0EsSUFBTU8sZUFBZSxHQUFHUCw2Q0FBQyxDQUFDLGlCQUFELENBQXpCO0FBRUEsSUFBSVEsSUFBSSxHQUFHLE1BQVg7QUFFQTs7QUFDQUMsY0FBYyxHQUFHQyxJQUFqQixDQUFzQixVQUFTQyxRQUFULEVBQW1CO0FBQ3ZDLE1BQUlDLFFBQVEsR0FBR0QsUUFBUSxDQUFDQyxRQUF4QjtBQUNBQSxVQUFRLENBQUNDLE9BQVQsQ0FBaUIsVUFBQUMsT0FBTyxFQUFJO0FBQzFCLFFBQUlDLE9BQU8sR0FBR2YsNkNBQUMsQ0FBQyxVQUFELENBQUQsQ0FBY2dCLElBQWQsQ0FBbUIsT0FBbkIsRUFBNEJGLE9BQTVCLEVBQXFDRyxJQUFyQyxDQUEwQ0gsT0FBMUMsQ0FBZDtBQUNBVixtQkFBZSxDQUFDYyxNQUFoQixDQUF1QkgsT0FBdkI7QUFDRCxHQUhEO0FBSUQsQ0FORCxXQU1TSSxPQUFPLENBQUNDLEtBTmpCO0FBUUFkLFlBQVksQ0FBQ2UsRUFBYixDQUFnQixRQUFoQixFQUEwQixZQUFXO0FBQ25DLE1BQUlDLFFBQVEsR0FBR3RCLDZDQUFDLENBQUMsSUFBRCxDQUFELENBQVF1QixHQUFSLEVBQWY7O0FBQ0EsTUFBR0QsUUFBUSxJQUFJLE1BQWYsRUFDQTtBQUNFZCxRQUFJLEdBQUcsTUFBUDtBQUNBSixtQkFBZSxDQUFDb0IsR0FBaEIsQ0FBb0IsU0FBcEIsRUFBK0IsTUFBL0I7QUFDQXpCLGlCQUFhLENBQUN5QixHQUFkLENBQWtCLFNBQWxCLEVBQTZCLE9BQTdCO0FBQ0FqQixtQkFBZSxDQUFDaUIsR0FBaEIsQ0FBb0IsU0FBcEIsRUFBK0IsT0FBL0I7QUFDRCxHQU5ELE1BT0ssSUFBR0YsUUFBUSxJQUFJLFNBQWYsRUFDTDtBQUNFZCxRQUFJLEdBQUcsU0FBUDtBQUNBSixtQkFBZSxDQUFDb0IsR0FBaEIsQ0FBb0IsU0FBcEIsRUFBK0IsT0FBL0I7QUFDQXpCLGlCQUFhLENBQUN5QixHQUFkLENBQWtCLFNBQWxCLEVBQTZCLE1BQTdCO0FBQ0FqQixtQkFBZSxDQUFDaUIsR0FBaEIsQ0FBb0IsU0FBcEIsRUFBK0IsTUFBL0I7QUFDRDtBQUNGLENBaEJEO0FBa0JBOztBQUNBdkIsT0FBTyxDQUFDb0IsRUFBUixDQUFXLE9BQVgsRUFBb0IsVUFBU0ksQ0FBVCxFQUFZO0FBQzlCQSxHQUFDLENBQUNDLGNBQUY7QUFDQSxNQUFJQyxXQUFXLEdBQUcsRUFBbEI7QUFDQW5CLE1BQUksSUFBSSxNQUFSLEdBQWlCbUIsV0FBVyxHQUFHNUIsYUFBYSxDQUFDd0IsR0FBZCxFQUEvQixHQUFxREksV0FBVyxHQUFHdkIsZUFBZSxDQUFDbUIsR0FBaEIsRUFBbkU7QUFDQSxNQUFJSyxHQUFHLEdBQUdyQixlQUFlLENBQUNnQixHQUFoQixFQUFWO0FBQ0FyQixhQUFXLENBQUMyQixJQUFaLENBQWlCLEVBQWpCO0FBQ0ExQixTQUFPLENBQUNxQixHQUFSLENBQVksU0FBWixFQUF1QixPQUF2QjtBQUNBTSxpQkFBZSxDQUFDSCxXQUFELEVBQWNuQixJQUFkLEVBQW9Cb0IsR0FBcEIsQ0FBZixDQUF3Q2xCLElBQXhDLENBQTZDLFVBQVNxQixHQUFULEVBQWM7QUFFekQ1QixXQUFPLENBQUNxQixHQUFSLENBQVksU0FBWixFQUF1QixNQUF2QjtBQUNBTyxPQUFHLENBQUNDLElBQUosQ0FBU25CLE9BQVQsQ0FBaUIsVUFBQW9CLElBQUksRUFBSTtBQUN2QixVQUFJSixJQUFJLDhIQUd1Q0ksSUFBSSxDQUFDQyxRQUg1QyxtQkFHNkQxQixJQUg3RCxtQkFHMEVtQixXQUgxRSxrQkFHNkZDLEdBSDdGLGdCQUdxR0ssSUFBSSxDQUFDRSxLQUgxRyxtQkFHd0hGLElBQUksQ0FBQ0MsUUFIN0gsd0RBSXFCRCxJQUFJLENBQUNHLE1BSjFCLHNFQUFSO0FBT0VsQyxpQkFBVyxDQUFDZ0IsTUFBWixDQUFtQlcsSUFBbkI7QUFDSCxLQVREO0FBV0F4QixzQkFBa0IsQ0FBQ1ksSUFBbkIsV0FBMkJjLEdBQUcsQ0FBQ00sYUFBL0IsMkNBQXVFTixHQUFHLENBQUNPLGFBQTNFO0FBQ0QsR0FmRDtBQWdCRCxDQXZCRDs7QUF5QkEsU0FBU1IsZUFBVCxDQUF5QlMsS0FBekIsRUFBZ0MvQixJQUFoQyxFQUFzQ29CLEdBQXRDLEVBQTJDO0FBQ3pDLFNBQU8sSUFBSVksT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUN0QzFDLGlEQUFDLENBQUMyQyxJQUFGLENBQU87QUFDSkMsU0FBRyw4QkFBd0JwQyxJQUF4QixtQkFBcUMrQixLQUFyQyxrQkFBa0RYLEdBQWxELENBREM7QUFFSmlCLFVBQUksRUFBRyxLQUZIO0FBR0pDLGFBQU8sRUFBRyxpQkFBU2YsR0FBVCxFQUFhO0FBQ3JCVSxlQUFPLENBQUNWLEdBQUQsQ0FBUDtBQUNELE9BTEc7QUFNSlgsV0FBSyxFQUFHLGVBQVNXLEdBQVQsRUFBY2dCLE1BQWQsRUFBc0JDLEdBQXRCLEVBQTBCO0FBQ2hDTixjQUFNLENBQUNNLEdBQUQsQ0FBTjtBQUNEO0FBUkcsS0FBUDtBQVVELEdBWE0sQ0FBUDtBQVlEOztBQUVELFNBQVN2QyxjQUFULEdBQTBCO0FBQ3hCLFNBQU8sSUFBSStCLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFDdEMxQyxpREFBQyxDQUFDMkMsSUFBRixDQUFPO0FBQ0pDLFNBQUcsaUJBREM7QUFFSkMsVUFBSSxFQUFHLEtBRkg7QUFHSkMsYUFBTyxFQUFHLGlCQUFTZixHQUFULEVBQWE7QUFDckJVLGVBQU8sQ0FBQ1YsR0FBRCxDQUFQO0FBQ0QsT0FMRztBQU1KWCxXQUFLLEVBQUcsZUFBU1csR0FBVCxFQUFjZ0IsTUFBZCxFQUFzQkMsR0FBdEIsRUFBMEI7QUFDaENOLGNBQU0sQ0FBQ00sR0FBRCxDQUFOO0FBQ0Q7QUFSRyxLQUFQO0FBVUQsR0FYTSxDQUFQO0FBWUQsQzs7Ozs7Ozs7Ozs7QUNoR0QsdUMiLCJmaWxlIjoiYWR2YW5jZWQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgJCBmcm9tICdqcXVlcnknO1xyXG5cclxuaW1wb3J0ICcuLi9zdHlsZXMvYWR2YW5jZWQuY3NzJztcclxuXHJcbmNvbnN0ICRzZWFyY2hfaW5wdXQgPSAkKFwiI3NlYXJjaC1pbnB1dFwiKVxyXG5jb25zdCAkc2VhcmNoICAgICAgID0gJChcIiNzZWFyY2gtYnRuXCIpXHJcbmNvbnN0ICRyZXN1bHRfY3RuICAgPSAkKFwiI3Jlc3VsdHMtY29udGFpbmVyXCIpXHJcbmNvbnN0ICRsb2FkZXIgICAgICAgPSAkKFwiI2xvYWRlclwiKVxyXG5jb25zdCAkc3BlYWtlcl9zZWxlY3QgPSAkKFwiI3NwZWFrZXItc2VsZWN0XCIpXHJcbmNvbnN0ICRyZXN1bHRfbnVtYmVyX2N0biA9ICQoXCIucmVzdWx0LW51bWJlci1jb250YWluZXJcIilcclxuY29uc3QgJG1vZGVfc2VsZWN0ID0gJChcIiNtb2RlLXNlbGVjdFwiKVxyXG5jb25zdCAkYWxsX2FueV9zZWxlY3QgPSAkKFwiI2FsbC1hbnktc2VsZWN0XCIpXHJcblxyXG5sZXQgbW9kZSA9ICd0ZXJtJ1xyXG5cclxuLyogbG9hZGluZyBhbGwgc3BlYWtlcnMgZm9yIHNlYXJjaGluZyBieSBzcGVha2VycyAqL1xyXG5nZXRBbGxTcGVha2VycygpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcclxuICBsZXQgc3BlYWtlcnMgPSByZXNwb25zZS5zcGVha2Vyc1xyXG4gIHNwZWFrZXJzLmZvckVhY2goc3BlYWtlciA9PiB7XHJcbiAgICBsZXQgJG9wdGlvbiA9ICQoXCI8b3B0aW9uPlwiKS5hdHRyKFwidmFsdWVcIiwgc3BlYWtlcikudGV4dChzcGVha2VyKVxyXG4gICAgJHNwZWFrZXJfc2VsZWN0LmFwcGVuZCgkb3B0aW9uKVxyXG4gIH0pO1xyXG59KS5jYXRjaChjb25zb2xlLmVycm9yKVxyXG5cclxuJG1vZGVfc2VsZWN0Lm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcclxuICBsZXQgc2VsZWN0ZWQgPSAkKHRoaXMpLnZhbCgpXHJcbiAgaWYoc2VsZWN0ZWQgPT0gJ3Rlcm0nKVxyXG4gIHtcclxuICAgIG1vZGUgPSAndGVybSdcclxuICAgICRzcGVha2VyX3NlbGVjdC5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpXHJcbiAgICAkc2VhcmNoX2lucHV0LmNzcygnZGlzcGxheScsICdibG9jaycpXHJcbiAgICAkYWxsX2FueV9zZWxlY3QuY3NzKCdkaXNwbGF5JywgJ2Jsb2NrJylcclxuICB9XHJcbiAgZWxzZSBpZihzZWxlY3RlZCA9PSAnc3BlYWtlcicpXHJcbiAge1xyXG4gICAgbW9kZSA9ICdzcGVha2VyJ1xyXG4gICAgJHNwZWFrZXJfc2VsZWN0LmNzcygnZGlzcGxheScsICdibG9jaycpXHJcbiAgICAkc2VhcmNoX2lucHV0LmNzcygnZGlzcGxheScsICdub25lJylcclxuICAgICRhbGxfYW55X3NlbGVjdC5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpXHJcbiAgfVxyXG59KVxyXG5cclxuLyogRXZlbnQgbGlzdGVuZXJzICovXHJcbiRzZWFyY2gub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKSB7XHJcbiAgZS5wcmV2ZW50RGVmYXVsdCgpXHJcbiAgbGV0IGlucHV0X3ZhbHVlID0gJydcclxuICBtb2RlID09ICd0ZXJtJyA/IGlucHV0X3ZhbHVlID0gJHNlYXJjaF9pbnB1dC52YWwoKSA6IGlucHV0X3ZhbHVlID0gJHNwZWFrZXJfc2VsZWN0LnZhbCgpXHJcbiAgbGV0IGFsbCA9ICRhbGxfYW55X3NlbGVjdC52YWwoKVxyXG4gICRyZXN1bHRfY3RuLmh0bWwoXCJcIilcclxuICAkbG9hZGVyLmNzcyhcImRpc3BsYXlcIiwgXCJibG9ja1wiKVxyXG4gIGdldFdpdGhTdGVtbWluZyhpbnB1dF92YWx1ZSwgbW9kZSwgYWxsKS50aGVuKGZ1bmN0aW9uKHJlcykge1xyXG5cclxuICAgICRsb2FkZXIuY3NzKFwiZGlzcGxheVwiLCBcIm5vbmVcIilcclxuICAgIHJlcy5kYXRhLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgIGxldCBodG1sID1cclxuICAgICAgYDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XHJcbiAgICAgICAgICAgIDxoNSBjbGFzcz1cImNhcmQtdGl0bGVcIj48YSBocmVmPVwiZGV0YWlsLyR7aXRlbS5kb2N1bWVudH0/bW9kZT0ke21vZGV9JnRlcm09JHtpbnB1dF92YWx1ZX0mYWxsPSR7YWxsfVwiPiR7aXRlbS50aXRsZX08L2E+ICgke2l0ZW0uZG9jdW1lbnR9KTwvaDU+XHJcbiAgICAgICAgICAgIDxwIGNsYXNzPVwiY2FyZC10ZXh0XCI+JHtpdGVtLm51bWJlcn0gcGFyYWdyYXBoZXMgY29ycmVzcG9uZGFudC48L3A+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5gXHJcbiAgICAgICAgJHJlc3VsdF9jdG4uYXBwZW5kKGh0bWwpXHJcbiAgICB9KVxyXG5cclxuICAgICRyZXN1bHRfbnVtYmVyX2N0bi50ZXh0KGAke3Jlcy5yZXN1bHRfbnVtYmVyfSByw6lzdWx0YXRzIHRyb3V2w6lzIGRhbnMgJHtyZXMuZG9jdW1lbnRzX2xlbn0gZG9jdW1lbnRzYClcclxuICB9KVxyXG59KVxyXG5cclxuZnVuY3Rpb24gZ2V0V2l0aFN0ZW1taW5nKHRlcm1zLCBtb2RlLCBhbGwpIHtcclxuICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgJC5hamF4KHtcclxuICAgICAgIHVybCA6IGAvYXBpL3NlYXJjaC8/bW9kZT0ke21vZGV9JnRlcm09JHt0ZXJtc30mYWxsPSR7YWxsfWAsXHJcbiAgICAgICB0eXBlIDogJ0dFVCcsXHJcbiAgICAgICBzdWNjZXNzIDogZnVuY3Rpb24ocmVzKXtcclxuICAgICAgICAgcmVzb2x2ZShyZXMpXHJcbiAgICAgICB9LFxyXG4gICAgICAgZXJyb3IgOiBmdW5jdGlvbihyZXMsIHN0YXR1cywgZXJyKXtcclxuICAgICAgICAgcmVqZWN0KGVycilcclxuICAgICAgIH1cclxuICAgIH0pO1xyXG4gIH0pXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldEFsbFNwZWFrZXJzKCkge1xyXG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAkLmFqYXgoe1xyXG4gICAgICAgdXJsIDogYC9hcGkvc3BlYWtlcnNgLFxyXG4gICAgICAgdHlwZSA6ICdHRVQnLFxyXG4gICAgICAgc3VjY2VzcyA6IGZ1bmN0aW9uKHJlcyl7XHJcbiAgICAgICAgIHJlc29sdmUocmVzKVxyXG4gICAgICAgfSxcclxuICAgICAgIGVycm9yIDogZnVuY3Rpb24ocmVzLCBzdGF0dXMsIGVycil7XHJcbiAgICAgICAgIHJlamVjdChlcnIpXHJcbiAgICAgICB9XHJcbiAgICB9KTtcclxuICB9KVxyXG59XHJcbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiJdLCJzb3VyY2VSb290IjoiIn0=